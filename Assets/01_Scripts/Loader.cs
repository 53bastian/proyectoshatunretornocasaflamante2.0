using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Loader : MonoBehaviour
{
    public string sceneToLoad;
    public Vector3 newLastPosition;

    public void LoadScene()
    {
        GameManager.instance.SaveGame(newLastPosition, sceneToLoad);
        SceneManager.LoadScene(sceneToLoad);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            LoadScene();
        }
    }

}
