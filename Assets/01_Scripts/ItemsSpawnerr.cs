using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsSpawnerr : MonoBehaviour
{

    public float timeToSpawn;
    float currentTimeToSpawn = 0f;
    public List<GameObject> items;
    public bool spawnAtStart = true;
    public Transform leftPoint;
    public Transform rightPoint;

    // Start is called before the first frame update
    void Start()
    {
        if (spawnAtStart)
        {
            int r = Random.Range(0, items.Count);
            Vector3 pos = new Vector3(Random.Range(leftPoint.position.x, rightPoint.position.x),transform.position.y,0);
            Instantiate(items[r], pos, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTimeToSpawn < timeToSpawn)
        {
            currentTimeToSpawn += Time.deltaTime;
        }
        else
        {
            currentTimeToSpawn = 0;
            int r = Random.Range(0, items.Count);
            Vector3 pos = new Vector3(Random.Range(leftPoint.position.x, rightPoint.position.x), transform.position.y, 0);
            Instantiate(items[r], pos, Quaternion.identity);
        }

    }
}
