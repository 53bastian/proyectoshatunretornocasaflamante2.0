using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovil : MonoBehaviour
{
    public Transform target;
    public float speed;
    public bool randomMovement= false;
    public float targetOffSet=100;

    private Vector3 start, end, oldTarget;
    // Start is called before the first frame update
    void Start()
    {
        if (target != null)
        {
            target.parent = null;
            start = transform.position;
            end = target.position;
            oldTarget = target.position;
            if (randomMovement)
            {
                GetRandomTarget();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        if (target != null)
        {
            float fixedSpeed = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.position, fixedSpeed);
        }
        if (transform.position == target.position) 
        {
            if (randomMovement)
            {
                GetRandomTarget();
            }
            target.position = (target.position == start) ? end : start;
        }
    }

    public void GetRandomTarget()
    {
        end = new Vector3(oldTarget.x + Random.Range(-targetOffSet, targetOffSet), oldTarget.y + Random.Range(-targetOffSet, targetOffSet), oldTarget.z);
    }
}
