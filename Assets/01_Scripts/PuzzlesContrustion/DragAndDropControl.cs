using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragAndDropControl : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IPointerClickHandler {
    
    public Lamp lamp;
    public Image itemImg;
    public Vector3 startPosition;
    public int position;
    public bool canMove = true;
    public void Awake()
    {
        startPosition = transform.position;
    }
    public void OnBeginDrag(PointerEventData eventData) {
        if(canMove){
            // startPosition = new Vector2(transform.position.x, transform.position.y);
            itemImg.raycastTarget = false;
        }
        Debug.Log(name + " OnBeginDrag");
    }

    public void OnDrag(PointerEventData eventData) {
        if(canMove){
            transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Debug.Log(name + " OnDrag");
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if(canMove){
            // Debug.Log(eventData.pointerEnter.name);
            if(eventData.pointerEnter != null){
                DragAndDropControl another = eventData.pointerEnter.GetComponent<DragAndDropControl>();
                if(another != null){
                    if(position == another.position){
                        transform.position = another.transform.position;
                        canMove = false;
                        itemImg.raycastTarget = false;
                        another.itemImg.raycastTarget = false;
                        lamp.ClearPiece();
                    }
                    else
                    {
                        transform.position = startPosition;
                        itemImg.raycastTarget = true;
                    }
                }
                else {
                    transform.position = startPosition;
                    itemImg.raycastTarget = true;
                     }
            }else{
                transform.position = startPosition;
                itemImg.raycastTarget = true;
            }
            Debug.Log(name + " OnEndDrag");
        }
    }

    public void OnDrop(PointerEventData data){
        Debug.Log(name + " OnDrop");
    }

    public void OnPointerEnter(PointerEventData pointerEventData){
        Debug.Log(gameObject.name);
    }

    public void OnPointerClick(PointerEventData pointerEventData){
        //Use this to tell when the user right-clicks on the Button
        if (pointerEventData.button == PointerEventData.InputButton.Right) {
            Debug.Log(name + " Game Object Right Clicked!");
        }

        //Use this to tell when the user left-clicks on the Button
        if (pointerEventData.button == PointerEventData.InputButton.Left) {
            Debug.Log(name + " Game Object Left Clicked!");
        }
    }
}
