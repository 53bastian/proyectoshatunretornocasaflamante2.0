using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour
{
    public PlayerController player;
    public GameObject puzzle;
    public GameObject lights;
   //public Animator anim;
    public float manaCost = 5f;
    public TextMesh manaCostText;
    public bool closed=false;
    public int pieces=18;
    void Start()
    {
        manaCostText.text = manaCost.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            if (Input.GetKeyDown(KeyCode.Q) && !closed)
            {
                Action();
            }
        }
    }

    public virtual void Action()
    {
        if (PlayerController.instance.UpdateManaPoints(-manaCost))
        {
            puzzle.SetActive(true);
            Time.timeScale = 0;
        }
    }
    public void ClosePuzzle() 
    {
        puzzle.SetActive(false);
        Time.timeScale = 1;
    }
    public void ClearPiece()
    {
        pieces--;
        if (pieces==0)
        {
            closed = true;
            lights.SetActive(true);
            manaCostText.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController p = collision.GetComponent<PlayerController>();
        if (p != null)
        {
            player = p;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController p = collision.GetComponent<PlayerController>();
        if (p != null)
        {
            player = null;
        }
    }
}
