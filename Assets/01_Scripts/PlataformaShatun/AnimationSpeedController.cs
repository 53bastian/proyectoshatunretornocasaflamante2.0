using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSpeedController : MonoBehaviour
{
    public Animator anim;
    void Start()
    {
        if (PlayerController.instance !=null)
        {
            anim.speed= 1+1*PlayerController.instance.stats.speedModifier;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
