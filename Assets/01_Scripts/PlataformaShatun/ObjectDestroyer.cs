using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour
{
    public float timeToDestroy=5.0f;
    public Animator anim;
    void Start()
    {
        StartCoroutine(Destroyer());
    }
    
    IEnumerator Destroyer()
    {
        if (anim !=null)
        {
            anim.SetTrigger("Destroy");
        }
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(this.gameObject);
    }
}
