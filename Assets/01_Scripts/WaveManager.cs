using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WaveManager : MonoBehaviour
{
    public List<Wave> waves;
    public int currentWave = 0;
    public EnemySpawner leftSpawner;
    public EnemySpawner rightSpawner;
    public int currentEnemys = 0;
    public static WaveManager instance;
    public GameObject buttons;
    public Text txtWave;
    // Start is called before the first frame update

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        currentWave = 0;
        //SpawmEnemys();
        StartCoroutine(StartWave());
    }
    void SpawmEnemys()
    {

        List<GameObject> leftEnemys = new List<GameObject>();
        leftEnemys = waves[currentWave].leftEnemys;
        List<GameObject> rightEnemys = new List<GameObject>();
        rightEnemys = waves[currentWave].rightEnemys;
        if (Random.Range(0,10)>5)
        {
            leftEnemys.Add(waves[currentWave].bosses[Random.Range(0, waves[currentWave].bosses.Count)]);

        }
        else
        {
            rightEnemys.Add(waves[currentWave].bosses[Random.Range(0, waves[currentWave].bosses.Count)]);
        }
        leftSpawner.Init(leftEnemys);
        rightSpawner.Init(rightEnemys);
        currentEnemys = leftEnemys.Count + rightEnemys.Count;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DeleteEnemy()
    {
        currentEnemys--;
        if (currentEnemys <=0)
        {
            currentWave++;
            if (currentWave >=waves.Count)
            {
                EndGame(true);
            }
            else
            {
                StartCoroutine(StartWave());
            }
        }
    }
    public void EndGame(bool win)
    {
        if (win)
        {
            txtWave.text = "Ganaste las rondas ";
            txtWave.transform.parent.gameObject.SetActive(true);
            buttons.SetActive(true);
        }
        else
        {
            txtWave.text = "Perdiste las rondas ";
            txtWave.transform.parent.gameObject.SetActive(true);
            buttons.SetActive(true);
        }
    }
    public IEnumerator StartWave()
    {
        txtWave.text = "Ronda " + (currentWave + 1).ToString();
        txtWave.transform.parent.gameObject.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        txtWave.transform.parent.gameObject.SetActive(false);
        SpawmEnemys();
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void RestartGame()
    {
        SceneManager.LoadScene("Combat");
    }
}



[System.Serializable] 
public class Wave
{
    public List<GameObject> leftEnemys;
    public List<GameObject> rightEnemys;
    public List<GameObject> bosses;

}