using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleGame : MonoBehaviour
{
    int[,] puzzleResult = new int[3, 3] { { 1, 2, 3 },
                                           { 4, 5, 6 },
                                           { 7, 8, 9 } };
    public PuzzlePiece[] puzzlePieces;
    private PuzzlePiece[,] puzzleGrid = new PuzzlePiece[3, 3];
    public Vector2 emptyPiecePosition = new Vector2(2, 2);
    private Vector3 emptyPieceStartPosition;
    public GameObject anim;
    public bool puzzlesCompleted=false;

    private void Start()
    {
        int index = 0;
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                puzzleGrid[row, col] = puzzlePieces[index];
                puzzleGrid[row, col].SetPosition(new Vector2(col, row));
                index++;
            }
        }

        emptyPieceStartPosition = puzzleGrid[(int)emptyPiecePosition.y, (int)emptyPiecePosition.x].transform.position;
    }

    private void Update()
    {
        if (!puzzlesCompleted)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) && emptyPiecePosition.y > 0)
            {
                SwapPieces(new Vector2(emptyPiecePosition.x, emptyPiecePosition.y - 1));
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) && emptyPiecePosition.y < 2)
            {
                SwapPieces(new Vector2(emptyPiecePosition.x, emptyPiecePosition.y + 1));
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && emptyPiecePosition.x > 0)
            {
                SwapPieces(new Vector2(emptyPiecePosition.x - 1, emptyPiecePosition.y));
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow) && emptyPiecePosition.x < 2)
            {
                SwapPieces(new Vector2(emptyPiecePosition.x + 1, emptyPiecePosition.y));
            }
        }
    }

    private void SwapPieces(Vector2 targetPosition)
    {
        PuzzlePiece targetPiece = puzzleGrid[(int)targetPosition.y, (int)targetPosition.x];
        Vector3 targetPiecePos = targetPiece.transform.position;
        PuzzlePiece emptyPiece = puzzleGrid[(int)emptyPiecePosition.y, (int)emptyPiecePosition.x];

        // Move the target piece to the empty piece's position
        targetPiece.SetPosition(emptyPiecePosition);
        targetPiece.transform.position = emptyPiece.transform.position;
        targetPiece.SetInitialPosition();
        // Move the empty piece to the target piece's position
        emptyPiece.SetPosition(targetPosition);
        emptyPiece.transform.position = targetPiecePos;
        emptyPiece.SetInitialPosition();

        // Update puzzle grid positions
        puzzleGrid[(int)emptyPiecePosition.y, (int)emptyPiecePosition.x] = targetPiece;
        puzzleGrid[(int)targetPosition.y, (int)targetPosition.x] = emptyPiece;
        // Update empty piece position
        emptyPiecePosition = targetPosition;
        //Check Puzzle result
        CheckPuzzle();
    }

    public void Restart()
    {
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                puzzleGrid[row, col].transform.position = puzzleGrid[row, col].initialPosition;
                puzzleGrid[row, col].SetPosition(new Vector2(col, row));
            }
        }

        emptyPiecePosition = new Vector2(2, 2);
        emptyPieceStartPosition = puzzleGrid[(int)emptyPiecePosition.y, (int)emptyPiecePosition.x].transform.position;
    }

    void CheckPuzzle()
    {
        bool isCompleted = true;
        for (int f = 0; f < 3; f++)
        {
            for (int c = 0; c < 3; c++)
            {
                if (puzzleGrid[f, c].value != puzzleResult[f, c])
                {
                    isCompleted = false;
                    break;
                }
            }
            if (!isCompleted) break;
        }
        if (isCompleted)
        {
            puzzlesCompleted = true;
            anim.SetActive(true);
            Debug.Log("Congratulations! You solved the puzzle!");
        }
    }
}
