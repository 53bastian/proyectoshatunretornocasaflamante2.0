using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour
{
    public int value;
    public Vector2 position;
    public Vector3 initialPosition;

    public void SetPosition(Vector2 newPosition)
    {
        position = newPosition;
    }

    public void SetInitialPosition()

    {
        initialPosition = transform.position;
    }

    private void Awake()
    {
        initialPosition = transform.position;
    }
}
