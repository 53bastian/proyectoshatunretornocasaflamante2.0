using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActiveGO : MonoBehaviour
{
    public GameObject target;
    public bool stopPlayer=true;
    public string keyPP;
    public bool saveKey = false;
    public bool newGoState = true;
    public UnityEvent uEvent;
    //public GameObject intro;
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Return))
        //{
        //    intro.SetActive(false);
        //}
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (stopPlayer)
            {
                 PlayerController.instance.StopPlayer();
            }
            target.SetActive(newGoState);
            if (saveKey)
            {
                //PlayerPrefs.SetInt(keyPP,1);
                GameManager.instance.playerData.keysPP.Add(keyPP);
            }
            if (uEvent != null)
            {
                uEvent.Invoke();
            }
            Destroy(gameObject);
        }
    }
}
