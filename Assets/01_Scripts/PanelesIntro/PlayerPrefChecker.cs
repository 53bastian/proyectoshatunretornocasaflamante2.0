using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefChecker : MonoBehaviour
{
    public string keyPP;
    public bool stateIfNotKey = false;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey(keyPP))
        {
            gameObject.SetActive(stateIfNotKey);
        }
        else
        {
            gameObject.SetActive(!stateIfNotKey);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
