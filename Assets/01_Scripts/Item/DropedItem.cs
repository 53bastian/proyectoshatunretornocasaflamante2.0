using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropedItem : MonoBehaviour
{
//    public ItemName item;
    public float cuantity;
    public Item_SO item;
    public Transform objToShow=null;
    public int energyType;
    public bool artefacto = false;
    public bool powerItem=false;

    #region //Implementación al inventario los ítems de teletransportación(usables)-----
    private void OnTriggerEnter2D(Collider2D collisionFlama)
    {
        PlayerController player = collisionFlama.GetComponent<PlayerController>();
        if (player != null)
        {
            if (item != null)
            {
                if (item.itemName != "Mana")
                {
                    if (item.itemName == "Vida")
                    {
                        player.AddLife(5);
                        Destroyer();
                    }
                    else
                    {
                        player.stats.keys += (int)cuantity;
                        if (artefacto)
                        {
                            InventorySystem.instance.AddNewArtefact(item);
                        }
                        else if (powerItem)
                        {
                            InventorySystem.instance.AddNewPowerItem(item); //almacena un item
                        }
                        else
                        {
                            InventorySystem.instance.AddNewItem(item); //Almacena items usables---
                        }
                        Destroyer();
                    }

                }
                else
                {
                    if (player.UpdateManaPoints(cuantity))
                    {
                        Destroyer();
                    }
                }
            }
            else {
                player.AddEnergy(energyType);
                Destroyer();
            }
        }
    }
    #endregion
    void Destroyer()
    {
        if (objToShow != null)
        {
            CamaraController.instance.ShowAnotherObject(objToShow);
        }
        Destroy(this.gameObject);

    }
}
public enum ItemName{
    Mana,
    Key
}