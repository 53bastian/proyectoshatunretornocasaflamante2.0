﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IPointerClickHandler  {

    public Image itemIMG;
    public Text itemQuantityTxt;
    public Text itemNameTxt;
    public Item_SO item = null;
    Vector3 startPosition;

    void Awake() {
        startPosition = GetComponent<RectTransform>().localPosition;
    }

    public void Init(Item_SO itemInfo){
        //initializing the item
        item = new Item_SO();
        //enable the image and the text
        itemIMG.gameObject.SetActive(true);
        itemQuantityTxt.gameObject.SetActive(true);
        itemNameTxt.gameObject.SetActive(true);
        //save information of the item
        item.itemName = itemInfo.itemName;
        item.itemQuantity = itemInfo.itemQuantity;
        item.itemSprite = itemInfo.itemSprite;
        item.itemCost = itemInfo.itemCost;
        item.action = itemInfo.action;
        item.canCombine = itemInfo.canCombine;
        //show the information of the item
        itemIMG.sprite = item.itemSprite;
        itemQuantityTxt.text = item.itemQuantity.ToString();
        itemNameTxt.text = item.itemName;
    }
    public void Init(ItemInfo itemInfo)
    {
        //initializing the item
        item = new Item_SO();
        //enable the image and the text
        itemIMG.gameObject.SetActive(true);
        itemQuantityTxt.gameObject.SetActive(true);
        itemNameTxt.gameObject.SetActive(true);
        //save information of the item
        item.itemName = itemInfo.itemName;
        item.itemQuantity = itemInfo.itemQuantity;
        item.itemSprite = itemInfo.itemSprite;
        item.itemCost = itemInfo.itemCost;
        item.action = itemInfo.action;
        item.canCombine = itemInfo.canCombine;
        //show the information of the item
        itemIMG.sprite = item.itemSprite;
        itemQuantityTxt.text = item.itemQuantity.ToString();
        itemNameTxt.text = item.itemName;
    }
    public void ResetItem(){
        //restarting the item
        item = null;
        //enable the image and the text
        itemIMG.gameObject.SetActive(false);
        itemQuantityTxt.gameObject.SetActive(false);
        itemNameTxt.gameObject.SetActive(false);
        //hide the information of the item
        itemIMG.sprite = null;
        itemQuantityTxt.text = "";
        itemNameTxt.text = "";
    }

    public void UpdateQuantity(int quantity){
        item.itemQuantity += quantity;
        itemQuantityTxt.text = item.itemQuantity.ToString();
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if(item != null){
            GetComponent<Image>().raycastTarget = false;
            Debug.Log(name + " OnBeginDrag");
        }
    }

    public void OnDrag(PointerEventData eventData) {
        if(item != null){
            transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Debug.Log(name + " OnDrag");
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if(item != null){
            if(eventData.pointerEnter != null){
                ItemUI auxItemUI = eventData.pointerEnter.GetComponent<ItemUI>();
                if(auxItemUI != null){
                    if(auxItemUI.item == null){
                        auxItemUI.Init(item);//save the item info to the new slot
                        ResetItem();//reset the item info
                    }else {
                        Item_SO auxItem = item;
                        Init(auxItemUI.item);
                        auxItemUI.Init(auxItem);
                    }
                }
            }
            GetComponent<RectTransform>().localPosition = startPosition;
            GetComponent<Image>().raycastTarget = true;
            Debug.Log(name + " OnEndDrag");
        }
    }

    public void OnDrop(PointerEventData data){
        Debug.Log(name + " OnDrop");
    }

    public void OnPointerEnter(PointerEventData pointerEventData){
        Debug.Log(gameObject.name);
    }

    public void OnPointerClick(PointerEventData pointerEventData){
        //Use this to tell when the user right-clicks on the Button
        if (pointerEventData.button == PointerEventData.InputButton.Right) {
            Debug.Log(name + " Game Object Right Clicked!");
        }
        //Use this to tell when the user left-clicks on the Button
        if (pointerEventData.button == PointerEventData.InputButton.Left) {
            Debug.Log(name + " Game Object Left Clicked!");
            
            if (item != null && item.itemQuantity > 0)
            {
                if (item.action!="")
                {
                    PlayerController.instance.ActivateItem(item);
                }
            }
        }
    }
}