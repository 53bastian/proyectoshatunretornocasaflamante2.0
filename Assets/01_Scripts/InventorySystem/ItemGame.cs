﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGame : MonoBehaviour {

    public Item_SO itemInfo;

    void Start() {
        GetComponent<SpriteRenderer>().sprite = itemInfo.itemSprite;
    }

    public void ChangeItem(Item_SO newInfo){
        itemInfo = newInfo;
    }

    void OnTriggerEnter2D(Collider2D other) {
        // Debug.Log(other.gameObject.name);
        // Debug.Log(other.gameObject.tag);
        if(other.gameObject.tag == "Player"){
            InventorySystem.instance.AddNewItem(itemInfo);
            Destroy(this.gameObject);
        }
    }
}