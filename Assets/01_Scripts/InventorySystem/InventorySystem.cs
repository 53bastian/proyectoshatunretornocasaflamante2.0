﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Power
{
    public string powerName;
    public Item_SO item1;
    public Item_SO item2;
    public Item_SO powerItem;
    public Sprite sprite;
    public int powerCode;

    public bool ContainItem(Item_SO itemA, Item_SO itemB)
    {
        if ((item1.itemName==itemA.itemName && item2.itemName== itemB.itemName) || (item1.itemName == itemB.itemName && item2.itemName == itemA.itemName))
        {
            return true;
        }
        return false;
    }
}

[System.Serializable]
public class ItemFromEnergy
{
    public Item_SO item;
    public int energy1;
    public int energy2;
    public Sprite sprite;
    public GameObject tutorialPanel;
    //public int powerCode;

    public bool ContainItem(int energyA, int energyB)
    {
        if ((energy1 == energyA && energy2 == energyB) || (energy1 == energyB && energy2 == energyA))
        {
            return true;
        }
        return false;
    }
}


public class InventorySystem : MonoBehaviour {
    
    public List<Item_SO> allItems;
    public List<ItemUI> itemsList;          //energias
    public List<ItemUI> itemsArtfct;        //reliquias
    public List<ItemUI> itemPowerUpList;
    public List<Item_SO> combinableItems;
    //
    public List<ItemUI> itemsListEsencia;
    public List<ItemUI> itemsListPiedras;
    //public List<ItemUI> itemsListReliquias;
    public List<ItemUI> itemsListRocasFirguras;
    public List<ItemUI> itemsListLlaves;
    public List<ItemUI> itemsListTesorosLegendarios;
    public List<ItemUI> itemsListSecreto;
    



    public GameObject panelInventario;
    public GameObject panelPower;     //combinar
    public GameObject panelEnergias;  //combinar

    public GameObject panelInventarioEsencia;
    public GameObject panelInventarioPiedras;
    public GameObject panelInventarioPoderes;
    //public GameObject panelInventarioUsables;
    //public GameObject panelUsables;
    //public GameObject panelInventarioReliquias;
    //public GameObject panelReliquias;
    public GameObject panelInventarioRocasFiguras;
    public GameObject panelInventarioLlaves;
    public GameObject panelInventarioTesorosLegendarios;
    public GameObject panelInventarioSecreto;



    public static InventorySystem instance;
    public Item_SO itemMagma;
    public Image leftItem;
    public Image rightItem;
    public int leftIndex=0;
    public int rightIndex = 0;
    int itemsCount = -1;
    int combinableItemsCount=-1;
    int itemsCountArtefacts = -1;
    int itemsCountPowerUp = -1;
    public List<Power> powers;
    public Image powerIMG;
    //energias
    public Image leftItemEnergia;
    public Image rightItemEnergia;
    public int leftIndexEnergia = 0;
    public int rightIndexEnergia = 0;
    public Image itemIMG;
    public List<ItemFromEnergy> itemsFromEnergy;
    public List<Sprite> energySprites;
    public Text powerCostTxt;
    [Header("Tutorial")]
    public GameObject tutorialH;
    
    void Awake() {
        if(instance == null){
            instance = this;
        }
    }

    void Start() {
        rightItem.sprite = itemMagma.itemSprite;
        GenerateItems();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.I))
        {
            panelInventario.SetActive(!panelInventario.activeSelf);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            if (tutorialH != null)
            {
                tutorialH.SetActive(true);
                tutorialH = null;
            }
            leftItem.sprite = itemsList[leftIndex].itemIMG.sprite;
            if (itemsList[leftIndex].item != null)
            {
                powerCostTxt.text = "+" + itemsList[leftIndex].item.itemCost;
            }
            panelEnergias.SetActive(false);
            panelPower.SetActive(!panelPower.activeSelf);
        }

        if (panelPower.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {

                Debug.Log("NumberOne");
                leftIndex = GoNext(leftIndex, -1);
                leftItem.sprite = combinableItems[leftIndex].itemSprite;
                if (combinableItems[leftIndex] != null)
                {
                    powerCostTxt.text = "+" + combinableItems[leftIndex].itemCost;
                }
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Debug.Log("NumberTwo");
                leftIndex = GoNext(leftIndex, 1);
                leftItem.sprite = combinableItems[leftIndex].itemSprite;
                if (combinableItems[leftIndex] != null)
                {
                    powerCostTxt.text = "+" + combinableItems[leftIndex].itemCost;
                }
            }
            //if (Input.GetKeyDown(KeyCode.Alpha3))
            //{
            //    Debug.Log("NumberThree");
            //    rightIndex = GoNext(rightIndex, -1);
            //    rightItem.sprite = itemsList[rightIndex].itemIMG.sprite;
            //}

            //if (Input.GetKeyDown(KeyCode.Alpha4))
            //{
            //    Debug.Log("NumberFour");
            //    rightIndex = GoNext(rightIndex, 1);
            //    rightItem.sprite = itemsList[rightIndex].itemIMG.sprite;
            //}
            if (Input.GetKeyDown(KeyCode.T))
            {
                Debug.Log("NewPower");
                //Aqui se quita costo de mana de los items
                GeneratePower();
                //panelPower.SetActive(false);
            }
        }


        if (Input.GetKeyDown(KeyCode.G) )
        {
            leftItemEnergia.sprite = energySprites[leftIndex];
            rightItemEnergia.sprite = energySprites[rightIndex];

            panelPower.SetActive(false);
            panelEnergias.SetActive(!panelEnergias.activeSelf);

        }

        if (panelEnergias.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Debug.Log("NumberTwo");
                leftIndexEnergia = GoNextEnergy(leftIndexEnergia, -1);
                leftItemEnergia.sprite = energySprites[leftIndexEnergia];
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Debug.Log("NumberOne");
                leftIndexEnergia = GoNextEnergy(leftIndexEnergia, 1);
                leftItemEnergia.sprite = energySprites[leftIndexEnergia];
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                Debug.Log("NumberFour");
                rightIndexEnergia = GoNextEnergy(rightIndexEnergia, -1);
                rightItemEnergia.sprite = energySprites[rightIndexEnergia];
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Debug.Log("NumberThree");
                rightIndexEnergia = GoNextEnergy(rightIndexEnergia, 1);
                rightItemEnergia.sprite = energySprites[rightIndexEnergia];
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                Debug.Log("NewItem");
                //Aqui se quita costo de mana de los items
                GenerateEnergyItem();
                //panelPower.SetActive(false);
            }
        }
    }

    public void GeneratePower()
    {
        bool canGeneratePower=false;
        int p = 0;
        for (int i = 0; i < powers.Count; i++)
        {
            if (powers[i].ContainItem(itemsList[leftIndex].item, itemMagma))
            {
                canGeneratePower = true;
                p = i;
            }
        }

        if (SearchPowerItem(powers[p].powerItem))
        {
            if (PlayerController.instance.UpdateManaPoints(-(itemsList[leftIndex].item.itemCost + itemMagma.itemCost)))
            {
                canGeneratePower = false;
                PlayerController.instance.UnlockPower(powers[p].powerCode);
                powerIMG.sprite = powers[p].sprite;
            }
        }
        if (canGeneratePower)
        {
            Debug.Log((itemsList[leftIndex].item.itemCost + itemMagma.itemCost));
            if (PlayerController.instance.UpdateManaPoints(-(itemsList[leftIndex].item.itemCost + itemMagma.itemCost)))
            {
                powerIMG.sprite = powers[p].sprite;
                AddNewPowerItem(powers[p].powerItem);
                PlayerController.instance.UnlockPower(powers[p].powerCode);
            }
        }   
    }

    public void GenerateEnergyItem()
    {
        bool canGenerateEnergyItem = false;
        int p = 0;
        for (int i = 0; i < itemsFromEnergy.Count; i++)
        {
            if (itemsFromEnergy[i].ContainItem(leftIndexEnergia, rightIndexEnergia))
            {
                canGenerateEnergyItem = true;
                p = i;
            }
        }
        if (canGenerateEnergyItem)
        {
            //Debug.Log((itemsList[leftIndex].item.itemCost + itemsList[leftIndex].item.itemCost));
            if (PlayerController.instance.UpdateEnergys(leftIndexEnergia, rightIndexEnergia))
            {
                if (itemsFromEnergy[p].tutorialPanel != null)
                {
                    itemsFromEnergy[p].tutorialPanel.SetActive(true);
                    itemsFromEnergy[p].tutorialPanel = null;
                }
                itemIMG.sprite = itemsFromEnergy[p].sprite;
                //PlayerController.instance.UnlockPower(powers[p].powerCode);
                AddNewItem(itemsFromEnergy[p].item);
            }
        }

    }

    public int GoNext(int index, int value)
    {
        index += value;

        if (index> combinableItemsCount)
        {
            index = 0;
        }
        else if (index <0)
        {
            index = combinableItemsCount;
        }
        
        return index;
    }

    public int GoNextEnergy(int index, int value)
    {
        index += value;
        if (index >= energySprites.Count)
        {
            index = 0;
        }
        if (index < 0)
        {
            index = energySprites.Count-1;
        }
        return index;
    }

    void GenerateItems(){
        //for (int i = 0; i < allItems.Count; i++){
        //    itemsList[i].Init(allItems[i]);
        //    itemsCount++;
        //}
        for (int i = 0; i < GameManager.instance.playerData.itemsList.Count; i++)
        {
            for (int j = 0; j < allItems.Count; j++)
            {
                if (allItems[j].itemName== GameManager.instance.playerData.itemsList[i].itemName)
                {
                    GameManager.instance.playerData.itemsList[i].itemSprite = allItems[j].itemSprite;
                    break;
                }
            }
            itemsList[i].Init(GameManager.instance.playerData.itemsList[i]);
            if (itemsList[i].item.canCombine)
            {
                combinableItems.Add(itemsList[i].item);
                combinableItemsCount++;
            }
            itemsCount++;
        }
        for (int i = 0; i < GameManager.instance.playerData.itemsArtfct.Count; i++)
        {
            for (int j = 0; j < allItems.Count; j++)
            {
                if (allItems[j].itemName == GameManager.instance.playerData.itemsArtfct[i].itemName)
                {
                    GameManager.instance.playerData.itemsArtfct[i].itemSprite = allItems[j].itemSprite;
                    break;
                }
            }
            itemsArtfct[i].Init(GameManager.instance.playerData.itemsArtfct[i]);
            itemsCountArtefacts++;
        }
        for (int i = 0; i < GameManager.instance.playerData.itemPowerUpList.Count; i++)
        {
            for (int j = 0; j < allItems.Count; j++)
            {
                if (allItems[j].itemName == GameManager.instance.playerData.itemPowerUpList[i].itemName)
                {
                    GameManager.instance.playerData.itemPowerUpList[i].itemSprite = allItems[j].itemSprite;
                    break;
                }
            }
            itemPowerUpList[i].Init(GameManager.instance.playerData.itemPowerUpList[i]);
            itemsCountPowerUp++;
        }
    }

    #region //Items
    public void AddNewItem(Item_SO newItem){
        int emptyPosition = -1;
        Debug.Log("agregoItemUsable");
        for (int i = 0; i < itemsList.Count; i++) {
            if(itemsList[i].item == null && emptyPosition == -1){
                emptyPosition = i;
                // itemsList[i].Init(newItem);
                // return;
            }
            if(itemsList[i].item != null) {
                if(itemsList[i].item.itemName == newItem.itemName){
                    itemsList[i].UpdateQuantity(1);
                    // itemsList[i].item.itemQuantity += newItem.itemQuantity;
                    // itemsList[i].itemQuantity.text = itemsList[i].item.itemQuantity.ToString();
                    GameManager.instance.AddItem(itemsList[i].item, 1);
                    return;
                }
            }
        }
        if(emptyPosition != -1){
            itemsList[emptyPosition].Init(newItem);
            itemsCount++;
            if (newItem.canCombine)
            {
                combinableItems.Add(itemsList[emptyPosition].item);
                combinableItemsCount++;
            }
            GameManager.instance.AddItem(itemsList[emptyPosition].item);
        }
        //GameSaveManager.instance.SaveGame();
    }
    #endregion
    public void AddNewArtefact(Item_SO newItem)
    {
        int emptyPosition = -1;
        for (int i = 0; i < itemsArtfct.Count; i++)
        {
            if (itemsArtfct[i].item == null && emptyPosition == -1)
            {
                emptyPosition = i;
                // itemsList[i].Init(newItem);
                // return;
            }
            if (itemsArtfct[i].item != null)
            {
                if (itemsArtfct[i].item.itemName == newItem.itemName)
                {
                    itemsArtfct[i].UpdateQuantity(1);
                    GameManager.instance.AddItemArtefact(itemsArtfct[i].item, 1);
                    // itemsList[i].item.itemQuantity += newItem.itemQuantity;
                    // itemsList[i].itemQuantity.text = itemsList[i].item.itemQuantity.ToString();
                    return;
                }
            }
        }
        if (emptyPosition != -1)
        {
            itemsArtfct[emptyPosition].Init(newItem);
            itemsCountArtefacts++;
            GameManager.instance.AddItemArtefact(itemsArtfct[emptyPosition].item);

        }
    }

    public bool RemoveItem(Item_SO newItem)
    {
        for (int i = 0; i < itemsList.Count; i++)
        {
            if (itemsList[i].item != null)
            {
                if (itemsList[i].item.itemName == newItem.itemName)
                {
                    if (itemsList[i].item.itemQuantity > 0)
                    {
                        itemsList[i].UpdateQuantity(-1);
                        GameManager.instance.AddItem(itemsList[i].item, -1);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool removeArtefact(Item_SO newItem)
    {
        for (int i = 0; i < itemsArtfct.Count; i++)
        {
            if (itemsArtfct[i].item != null)
            {
                if (itemsArtfct[i].item.itemName == newItem.itemName)
                {
                    if (itemsArtfct[i].item.itemQuantity > 0)
                    {
                        Debug.Log("RemoveArtefact");
                        itemsArtfct[i].UpdateQuantity(-1);
                        GameManager.instance.AddItem(itemsArtfct[i].item, -1);
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public bool RemoveItemPowerUp(Item_SO newItem)
    {
        for (int i = 0; i < itemPowerUpList.Count; i++)
        {
            if (itemPowerUpList[i].item != null)
            {
                if (itemPowerUpList[i].item.itemName == newItem.itemName)
                {
                    if (itemPowerUpList[i].item.itemQuantity > 0)
                    {
                        itemPowerUpList[i].UpdateQuantity(-1);
                        GameManager.instance.AddItemPowerUp(itemPowerUpList[i].item, -1);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void GenerateRandomItem(){
        int r = Random.Range(0, allItems.Count);
        AddNewItem(allItems[r]);
    }

    #region
    public bool SearchPowerItem(Item_SO newItem)
    {
        
        for (int i = 0; i < itemPowerUpList.Count; i++)
        {
           
            if (itemPowerUpList[i].item != null)
            {
                if (itemPowerUpList[i].item.itemName == newItem.itemName)
                {
                    Debug.Log("encontro el item");
                    return true;
                }
            }
        }
        return false;
    }
    #endregion

    public bool SearchPowerItemQuantity(Item_SO newItem, int quantity)
    {

        for (int i = 0; i < itemPowerUpList.Count; i++)
        {

            if (itemPowerUpList[i].item != null)
            {
                if (itemPowerUpList[i].item.itemName == newItem.itemName)
                {
                   
                    return itemPowerUpList[i].item.itemQuantity >= quantity;
                }
            }
        }
        return false;
    }


    public bool SearchArtefact(Item_SO newItem)
    {

        for (int i = 0; i < itemsArtfct.Count; i++)
        {

            if (itemsArtfct[i].item != null)
            {
                if (itemsArtfct[i].item.itemName == newItem.itemName)
                {
                    Debug.Log("encontro el itemArtefact");
                    return true;
                }
            }
        }
        return false;
    }

    #region
    public void AddNewPowerItem(Item_SO newItem)
    {
        int emptyPosition = -1;
        for (int i = 0; i < itemPowerUpList.Count; i++)
        {
            if (itemPowerUpList[i].item == null && emptyPosition == -1)
            {
                emptyPosition = i;
                // itemsList[i].Init(newItem);
                // return;
            }
            if (itemPowerUpList[i].item != null)
            {
                if (itemPowerUpList[i].item.itemName == newItem.itemName)
                {
                    itemPowerUpList[i].UpdateQuantity(1);
                    GameManager.instance.AddItemPowerUp(itemPowerUpList[i].item, 1);
                    // itemsList[i].item.itemQuantity += newItem.itemQuantity;
                    // itemsList[i].itemQuantity.text = itemsList[i].item.itemQuantity.ToString();
                    return;
                }
            }
        }
        if (emptyPosition != -1)
        {
            itemsCountPowerUp++;
            itemPowerUpList[emptyPosition].Init(newItem);
            GameManager.instance.AddItemPowerUp(itemPowerUpList[emptyPosition].item);
        }
    }
    #endregion
}