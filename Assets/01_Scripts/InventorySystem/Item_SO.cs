﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item")]
public class Item_SO : ScriptableObject {

    public string itemName;
    public int itemQuantity = 1;
    public float itemCost;
    public Sprite itemSprite;
    public string action;
    public bool canCombine=false;
}

[System.Serializable]
public class ItemInfo
{
    public string itemName;
    public int itemQuantity;
    public float itemCost;
    public Sprite itemSprite;
    public string action;
    public bool canCombine=false;
}