using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocaMagmaRun : RocaMagmaStateMachine
{
    public float timer = 0.0f;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        //timer = zafiro.walkingTime;
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (roca.distance <= roca.meleeAttackRange)
        {
            if (Random.Range(1, 101) <= roca.stonChanc)
            {
                animator.SetTrigger("StonAttack");
            }
            else
            {
                animator.SetTrigger("meleeAttack");
            }
            return;
        }
        //else if (roca.distance <= 12)
        //{
        //    animator.SetTrigger("rangeAttack");
        //    return;
        //}
        roca.Move();
        //if (timer > 0f)
        //{
        //    timer -= Time.deltaTime;
        //}
        //else
        //{
        //    animator.SetBool("walking", false);
        //}
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //zafiro.rb.velocity = Vector2.zero;
        //animator.transform.localScale = new Vector3(animator.transform.localScale.x * (-1), 1, 1);
        //zafiro.lifeBar.transform.localScale = new Vector3(animator.transform.localScale.x, 1, 1);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
