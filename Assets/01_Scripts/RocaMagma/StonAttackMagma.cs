using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StonAttackMagma : RocaMagmaStateMachine
{ // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public Vector3 target;
    public Vector3 targetPos;
    public Vector3 pos;
    public bool attacking= false;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        targetPos = new Vector3(PlayerController.instance.transform.position.x, 0, 0) ;
        if (targetPos.x < animator.transform.position.x)
        {
            pos = new Vector3(roca.limits[1].position.x,0,0);
            //pos = animator.transform.position + new Vector3(10, 0, 0);
        }
        else
        {
            pos = new Vector3(roca.limits[0].position.x, 0, 0);
            //pos = animator.transform.position + new Vector3(-10, 0, 0);
        }
        target = pos;
        if (target.x < animator.transform.position.x)
        {
            animator.transform.localScale = new Vector3((-1) * Mathf.Abs(animator.transform.localScale.x), animator.transform.localScale.y, animator.transform.localScale.z);
        }
        else if (target.x > animator.transform.position.x)
        {
            animator.transform.localScale = new Vector3(Mathf.Abs(animator.transform.localScale.x), animator.transform.localScale.y, animator.transform.localScale.z);
        }
        //animator.transform.localScale = new Vector3(animator.transform.localScale.x *(-1),animator.transform.localScale.y, animator.transform.localScale.z);
        roca.rb.velocity = new Vector2(20 * Mathf.Sign(animator.transform.localScale.x), 0);
        //roca.MeleeAttack();
        //bossRoca.timer = 5;
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (target.x < animator.transform.position.x)
        {
           animator.transform.localScale = new Vector3((-1) * Mathf.Abs(animator.transform.localScale.x), animator.transform.localScale.y, animator.transform.localScale.z);
            roca.rb.velocity = new Vector2(20 * Mathf.Sign(animator.transform.localScale.x), 0);
        }
        else if (target.x > animator.transform.position.x)
        {
            animator.transform.localScale = new Vector3(Mathf.Abs(animator.transform.localScale.x), animator.transform.localScale.y, animator.transform.localScale.z);
            roca.rb.velocity = new Vector2(20 * Mathf.Sign(animator.transform.localScale.x), 0);
        }
        float d = Vector3.Distance(new Vector3(animator.transform.position.x,0,0), target);
        Debug.Log("d " +d);
        if (d <= 1)
        {
            if (target==pos)
            {
                target = targetPos;
                roca.rb.velocity = Vector2.zero;
                //animator.transform.localScale = new Vector3(animator.transform.localScale.x * (-1), animator.transform.localScale.y, animator.transform.localScale.z);
                //attacking = true;
                Debug.Log("d");
            }
            else
            {
                roca.rb.velocity = Vector2.zero;
                animator.SetTrigger("StonAttack");
            }
        }
        //if (!attacking)
        //{
        //    float d1= Vector3.Distance(animator.transform.position,pos);
        //    Debug.Log("d1 :" + d1);
        //    if (d1 <= 1)
        //    {
        //        roca.rb.velocity = Vector2.zero;
        //        animator.transform.localScale = new Vector3(animator.transform.localScale.x * (-1), animator.transform.localScale.y, animator.transform.localScale.z);
        //        attacking = true;
        //        Debug.Log("d1");
        //    }
        //    else
        //    {
        //        Debug.Log("else d1");
        //        roca.rb.velocity = new Vector2(20 * Mathf.Sign(animator.transform.localScale.x), 0);
        //    }
        //}
        //else
        //{
        //    float d2 = Vector3.Distance(animator.transform.position, targetPos);
        //    if (d2 <= 1)
        //    {
        //        roca.rb.velocity = Vector2.zero;
        //        attacking = false;
        //        Debug.Log("d2");
        //        animator.SetTrigger("StonAttack");
        //    }
        //    else
        //    {
        //        Debug.Log("else");
        //        roca.rb.velocity = new Vector2(20 * Mathf.Sign(animator.transform.localScale.x), 0);
        //    }
        //}

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //tronco.rb.velocity = Vector2.zero;
        //animator.transform.localScale = new Vector3(animator.transform.localScale.x *(-1), 1,1);
        //tronco.lifeBar.transform.localScale  = new Vector3(animator.transform.localScale.x  , 1, 1);

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
