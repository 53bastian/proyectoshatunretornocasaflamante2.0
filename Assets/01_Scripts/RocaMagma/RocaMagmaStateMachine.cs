using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocaMagmaStateMachine : StateMachineBehaviour
{
    public IARocaMagma roca;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        roca = animator.gameObject.GetComponent<IARocaMagma>();
    }
}
