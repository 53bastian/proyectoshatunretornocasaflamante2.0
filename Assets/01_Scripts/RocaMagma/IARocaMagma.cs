using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
public class IARocaMagma : MonoBehaviour, IDamageable
{
    public AudioSource hitSound;
    public float hp= 100;
    float maxHP;
    public float speed = 10;
    public Rigidbody2D rb;
    public float walkingTime = 5f;
    public Animator animxd;
    public DamageTriger damageTriger;
    public Image lifeBar;
    public List<GameObject> dropedItems;
    public SpriteRenderer sprite;
    public float distance = 0f;
    public float timeBtwAttack= 2f;
    public Transform firePoint;
    public BulletBoss bulletPrefab;
    public float damage=15;
    public int proyectiles = 3;
    public GameObject platformBoss;
    public bool canTakeDamage=false;
    public float timer = 0.0f;
    public Animator protectedDoor = null;
    //
    public int currentTarget;
    [Header("0-left,1-center,2-right")]
    public List<Transform> targets;
    public List<Transform> limits;
    public int indexUpdater= -1;

    public Vector2 attackRange;
    public LayerMask enemysLayer;
    [Range(1,100)]public float stonChanc = 10;
    public float meleeAttackRange = 4f;
    public void Awake()
    {
        currentTarget = 1;
        targets[0].parent.SetParent(null);
    }
    public void UpdateTarget()
    {
        rb.velocity = Vector2.zero;
        currentTarget++;
        if (currentTarget >= targets.Count)
        {
            currentTarget = 0;
        }
        //Debug.Log("Entro a D" + currentTarget);
        //switch (currentTarget)
        //{
        //    case 0:
        //        indexUpdater *= -1;
        //        transform.localScale = new Vector3(transform.localScale.x * (-1), transform.localScale.y, transform.localScale.z);
        //        break;
        //    case 1:
        //        indexUpdater *= -1;
        //        transform.localScale = new Vector3(transform.localScale.x * (-1), transform.localScale.y, transform.localScale.z);
        //        break;
        //}
        //currentTarget += indexUpdater;
        //Debug.Log("newTarget" + currentTarget);
    }
    public void Move()
    {
        if (targets[currentTarget].position.x < transform.position.x)
        {
            transform.localScale = new Vector3((-1) * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            rb.velocity = Vector2.zero;
        }
        else if (targets[currentTarget].position.x > transform.position.x)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            rb.velocity = Vector2.zero;
        }
        Vector2 v1 = new Vector2(transform.position.x, 0);
        Vector2 v2 = new Vector2(targets[currentTarget].position.x, 0);

        float d = Vector2.Distance(v2, v1);
        //Debug.Log("target "+currentTarget+ " "+d);
        if (d <= 0.5f)
        {
            UpdateTarget();
            animxd.SetBool("walking",false);
            return;
        }
        rb.velocity = (transform.localScale.x > 0) ? Vector2.right*speed : Vector2.left*speed;
    }
    public void TakeDamage(float damage)
    {
        if (!canTakeDamage)
        {
            return;
        }
        if (hp == 0.0f)
        {
            return;
        }
        if (hp - damage > 0)
        {
            hitSound.Play();
            animxd.SetTrigger("Herido");
            //StartCoroutine(goRun());
        }
        hp -= damage;
        if (hp<=0)
        {
            hp = 0;
            Death();   
        }
        lifeBar.fillAmount = hp / maxHP;
    }

    void Death()
    {
        animxd.SetTrigger("Death");
        damageTriger.gameObject.SetActive(false);
        lifeBar.transform.parent.gameObject.SetActive(false);
        damageTriger.gameObject.SetActive(false);
        for (int i = 0; i < dropedItems.Count; i++)
        {
            Instantiate(dropedItems[i], transform.position + new Vector3(Random.Range(-0.5f, 0.5f), 0, 0),Quaternion.identity);
        }
        //Camera.main.GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize=5;
        if (protectedDoor != null)
        {
            protectedDoor.SetTrigger("On");
        }
        if (WaveManager.instance != null)
        {
            WaveManager.instance.DeleteEnemy();
            StartCoroutine(DestroyEnemy());
        }
        
        //Destroy(this.gameObject);
        StartCoroutine(ReStartNPC());
    }
    // Start is called before the first frame update
    void Start()
    {
        //animxd = GetComponent<Animator>();
        maxHP = hp;
        lifeBar.fillAmount = hp / maxHP;
    }

    // Update is called once per frame
    void Update()
    {
        distance = 100;
        if (PlayerController.instance.transform.position.y <= transform.position.y + 1 && PlayerController.instance.transform.position.y >= transform.position.y - 1)
        {
            Vector2 v1 = new Vector2(PlayerController.instance.transform.position.x, 0);
            Vector2 v2 = new Vector2(transform.position.x, 0);
            distance = Vector2.Distance(v1, v2);
        }
        
    }
    IEnumerator goRun()
    {
        yield return new WaitForSeconds(1.5f);
        animxd.SetBool("walking", true);
    }
    IEnumerator ReStartNPC()
    {
        yield return new WaitForSeconds(2f);
        sprite.enabled = false;
        yield return new WaitForSeconds(10f);
        hp = maxHP;
        lifeBar.fillAmount = hp / maxHP;
        animxd.SetTrigger("Resurection");
        lifeBar.transform.parent.gameObject.SetActive(true);
        damageTriger.gameObject.SetActive(true);
        sprite.enabled = true;

    }
    public void SpawnProyectil()
    {
        if (PlayerController.instance.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector3((-1)* Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        else if (PlayerController.instance.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        
        //Vector2 lookDir = PlayerController.instance.transform.position - firePoint.position;
        //float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        //firePoint.eulerAngles = new Vector3(0, 0, angle);
        BulletBoss bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        bullet.Init(damage);
        Debug.Log("Disapro");
    }
    public void Onda()
    {
        StartCoroutine(IniciarOnda());
    }
    IEnumerator IniciarOnda()
    {
        platformBoss.SetActive(true);
        yield return new WaitForSeconds(5f);
        platformBoss.SetActive(false);
        animxd.SetTrigger("nextState");
    }
    IEnumerator DestroyEnemy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }

    public void MeleeAttack()
    {
        //Debug.Log("Entro atacar");
        if (PlayerController.instance.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector3((-1) * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        else if (PlayerController.instance.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        Collider2D[] enemysToDamage = Physics2D.OverlapBoxAll(firePoint.position, attackRange, 0, enemysLayer);
        for (int i = 0; i < enemysToDamage.Length; i++)
        {
            PlayerController damageable = enemysToDamage[i].GetComponent<PlayerController>();
            if (damageable != null)
            {
                Debug.Log("Econtro un enemigo " + i);
                damageable.TakeDamage(damage);
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(firePoint.position, attackRange);
    }
}
