using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float timeToSpawn;
    float currentTimeToSpawn=0f;
    public List<GameObject> Enemys;
    public bool spawnAtStart = true;
    public bool initialized = false;
    public int index = 0;

    //// Start is called before the first frame update
    //void Start()
    //{
    //    if (spawnAtStart)
    //    {
    //        int r = Random.Range(0, Enemys.Count);
    //        Instantiate(Enemys[r], transform.position, Quaternion.identity);
    //    }
    //}

    // Update is called once per frame
    void Update()
    {
        if (initialized)
        {
            if (currentTimeToSpawn < timeToSpawn)
            {
                currentTimeToSpawn += Time.deltaTime;
            }
            else
            {
                currentTimeToSpawn = 0;
                
                Instantiate(Enemys[index], transform.position, Quaternion.identity);
                index++;
                if (index >=Enemys.Count)
                {
                    index = 0;
                    initialized = false;
                }
            }
        }

    }
    public void Init(List<GameObject> list)
    {
        index = 0;
        Enemys = list;
        initialized = true;
    }
}
