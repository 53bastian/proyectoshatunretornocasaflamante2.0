using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocaStateMachine : StateMachineBehaviour

{
    public IARoca roca;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        roca = animator.gameObject.GetComponent<IARoca>();
    }
}
