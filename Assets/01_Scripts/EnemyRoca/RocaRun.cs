using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocaRun : RocaStateMachine
{
    public float speed;
    public float distance;
    public float distanceToOrigin;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector2 v1;
        Vector2 v2;
        v1 = new Vector2(roca.origin.x, 0);
        v2 = new Vector2(roca.transform.position.x, 0);
        distance = Vector2.Distance(PlayerController.instance.transform.position, roca.transform.position);
        distanceToOrigin = Vector2.Distance(v1, v2);
        v1 = new Vector2(0, PlayerController.instance.transform.position.y);
        v2 = new Vector2(0, roca.transform.position.y);
        float distance2 = Vector2.Distance(v1, v2);
        //distance=Vector2.Distance(PlayerController.instance.transform.position, roca.transform.position);
        //distanceToOrigin = Vector2.Distance(roca.origin, roca.transform.position);

        if (distance <= roca.attackDistance)
        {
            Debug.Log("1");
            animator.SetBool("Hit",true);
            roca.rb.velocity = Vector2.zero;
            return;
        }
        else if((roca.distance>=15f || distance2 >=1) && animator.GetBool("NearPlayer"))
        {
            Debug.Log("2");
            animator.SetBool("FarFromHome",true);
            animator.SetBool("NearPlayer", false);
        }
        if (distanceToOrigin <= 2f && !animator.GetBool("NearPlayer"))
        {
            Debug.Log("3");
            animator.SetBool("FarFromHome", false);
            roca.rb.velocity = Vector2.zero;
            return;
        }

        roca.Move(animator.GetBool("FarFromHome"));
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
