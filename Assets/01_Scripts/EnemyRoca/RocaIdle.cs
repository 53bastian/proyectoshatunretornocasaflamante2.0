using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocaIdle : RocaStateMachine
{


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }



    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
            
    {
        Vector2 v1 = new Vector2(0, PlayerController.instance.transform.position.y);
        Vector2 v2 = new Vector2(0, roca.transform.position.y);
        float distance = Vector2.Distance(v1, v2);
        if (distance <= 1.5f && roca.distance <= 8f)
        {
            animator.SetBool("NearPlayer", true);
        }
        //if (distance<=1)
        //{
        //    if (roca.distance <= 8f)
        //    {
        //        animator.SetBool("NearPlayer", true);

        //    }
        //}
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
