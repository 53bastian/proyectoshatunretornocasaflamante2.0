using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "playerStats", menuName = "ScriptableObjetcs/playerStats")]
public class PlayerStats_SO : ScriptableObject
{
    //Item energia velocidades
    public float life = 100;
    public float maxMana = 200;
    public float currentMana = 0;
    [SerializeField] float speedMax = 5f;
    [SerializeField] float speed = 2f;
    [SerializeField] float timeBTWAttack = 0.5f;
    public float jumpForce = 200;
    public float damage = 50f;
    public float timeBTWTakeDamage = 2.0f;
    public int keys = 0;
    public float maxHp;
    //Energias
    public int e1 = 0;
    public int e2 = 0;
    public int e3 = 0;

    public float speedModifier = 0.1f;

    public float Speed { get { return (speed + speed * speedModifier); } set { } }
    public float SpeedMax { get { return (speedMax + speedMax * speedModifier); } set { } }
    public float TimeBTWAttack { get { return (timeBTWAttack - timeBTWAttack * speedModifier); } set { } }


}
