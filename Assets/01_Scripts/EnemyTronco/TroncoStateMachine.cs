using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroncoStateMachine : StateMachineBehaviour
{
    public IATronco tronco;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        tronco = animator.gameObject.GetComponent<IATronco>();
    }
}
