using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPlantaStateMachine : StateMachineBehaviour
{
    public IABossPlanta bossPlanta;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        bossPlanta = animator.gameObject.GetComponent<IABossPlanta>();
    }
}
