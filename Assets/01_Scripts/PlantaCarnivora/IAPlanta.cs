using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IAPlanta : MonoBehaviour, IDamageable
{
    public float distance = 0f;
    public Rigidbody2D rb;
    public float speed = 10;
    [HideInInspector]public Vector2 origin;
    public Vector3 attackRange;
    public Transform attackPos;
    public LayerMask enemysLayer;
    public float damage;
    public float hp = 100;
    float maxHP;
    public Image lifeBar;
    public Animator animxd;
    public List<GameObject> dropedItems;
    bool canInterrupt = true;
    public float timeBTWInterruption= 2.0f;
    float currentTimeBTWInterruption=0f;
    public GameObject damageTrigger;
    public SpriteRenderer sprite;
    public IABossPlanta bossParent = null;
    void Awake() 
    {
        origin = new Vector2(transform.position.x, transform.position.y);    
    }
    
    // Start is called before the first frame update
    void Start()
    {
        maxHP = hp;
        lifeBar.fillAmount = hp / maxHP;

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 v1 = new Vector2(PlayerController.instance.transform.position.x,0);
        Vector2 v2 = new Vector2(origin.x, 0);
        distance = Vector2.Distance(v1,v2);
        if (!canInterrupt)
        {
            if (currentTimeBTWInterruption < timeBTWInterruption)
            {
                currentTimeBTWInterruption += Time.deltaTime;
            }
            else
            {
                currentTimeBTWInterruption = 0;
                canInterrupt = true;
            }
        }
    }


    public void Move(bool farFromHome)
    {
        if (farFromHome)
            transform.localScale = (origin.x <= transform.position.x) ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
        else
            transform.localScale = (PlayerController.instance.transform.position.x <= transform.position.x) ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
        lifeBar.transform.localScale= (transform.localScale.x >= 0) ? new Vector3(+1, 1, 1) : new Vector3(-1, 1, 1);
        rb.velocity = (transform.localScale.x > 0) ? Vector2.right * speed : Vector2.left * speed;
    }
    public void MakeAttack()
    {
       //canAttack = false;
       // currentTimeBTWAttack = 0;
        //anim.SetTrigger("OnAttack");
        Collider2D[] enemysToDamage = Physics2D.OverlapBoxAll(attackPos.position, attackRange, 0, enemysLayer);
        for (int i = 0; i < enemysToDamage.Length; i++)
        {
            PlayerController damageable = enemysToDamage[i].GetComponent<PlayerController>();
            if (damageable != null)
            {
                damageable.TakeDamage((lifeBar.fillAmount<=0.3f ? 3*damage : damage));

            }
        }
    }
    public void TakeDamage(float damage)
    {
        if (hp==0.0f)
        {
            return;
        }
        if (hp - damage > 0 && canInterrupt)
        {
            animxd.SetTrigger("DamageRoca");
            canInterrupt = false;
            //StartCoroutine(goRun());
        }
        hp -= damage;
        if (hp <= 0)
        {
            hp = 0;
            Death();
        }
        lifeBar.fillAmount = hp / maxHP;
    }

    void Death()
    {
        animxd.SetTrigger("DeathRoca");
        lifeBar.transform.parent.gameObject.SetActive(false);
        damageTrigger.SetActive(false);
        //damageTriger.gameObject.SetActive(false);
        for (int i = 0; i < dropedItems.Count; i++)
        {
            Instantiate(dropedItems[i], transform.position + new Vector3(Random.Range(-0.5f, 0.5f), 0, 0), Quaternion.identity);
        }
        if (bossParent == null)
        {
            if (WaveManager.instance != null)
            {
                WaveManager.instance.DeleteEnemy();
                StartCoroutine(DestroyEnemy());
            }
            else
            {
                StartCoroutine(ReStartNPC());
            }
        }
        else
        {
            bossParent.ClearMinion();
            StartCoroutine(DeathNPC());
        }
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(attackPos.position, attackRange);
    }

    IEnumerator ReStartNPC()
    {
        yield return new WaitForSeconds(2f);
        sprite.enabled = false;
        yield return new WaitForSeconds(28f);
        hp = maxHP;
        lifeBar.fillAmount = hp / maxHP;
        animxd.SetTrigger("Resurection");
        lifeBar.transform.parent.gameObject.SetActive(true);
        damageTrigger.SetActive(true);
        sprite.enabled = true;

    }
    IEnumerator DeathNPC()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }

    IEnumerator DestroyEnemy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }
}