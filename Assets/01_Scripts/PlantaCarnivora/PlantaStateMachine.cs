using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantaStateMachine : StateMachineBehaviour

{
    public IAPlanta planta;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        planta = animator.gameObject.GetComponent<IAPlanta>();
    }
}
