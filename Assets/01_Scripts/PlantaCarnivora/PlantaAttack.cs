using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantaAttack : PlantaStateMachine
{
    //public float timer = 0.0f;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        planta.MakeAttack();
        //timer = Random.Range(3f, 4f);
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (timer > 0f)
        //{
        //    timer -= Time.deltaTime;
        //}
        //else
        //{
        //    animator.SetBool("Hit", false);
        //}
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("Hit", false);
    }
}
