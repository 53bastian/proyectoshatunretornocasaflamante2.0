using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportsTotemsManager : MonoBehaviour
{
    public GameObject teleportTurquesa;
    public GameObject teleportAmatista;
    public GameObject teleportOnixBlanco;
    public GameObject teleportAmbar;
    public GameObject teleportEsmeralda;
    public GameObject teleportJade;
    public static TeleportsTotemsManager instance;

    private void Awake()
    {
        if (instance ==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (PlayerPrefs.HasKey("TeleportAmbar"))
        {
            if (PlayerPrefs.GetString("TeleportAmbar")==sceneName)
            {
                Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportAmbarX"), PlayerPrefs.GetFloat("TeleportAmbarY"),0);
                Instantiate(teleportAmbar, pos, Quaternion.identity);
            }
        }
        if (PlayerPrefs.HasKey("TeleportTurquesa"))
        {
            if (PlayerPrefs.GetString("TeleportTurquesa") == sceneName)
            {
                Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportTurquesaX"), PlayerPrefs.GetFloat("TeleportTurquesaY"), 0);
                Instantiate(teleportTurquesa, pos, Quaternion.identity);
            }
        }
        if (PlayerPrefs.HasKey("TeleportAmatista"))
        {
            if (PlayerPrefs.GetString("TeleportAmatista") == sceneName)
            {
                Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportAmatistaX"), PlayerPrefs.GetFloat("TeleportAmatistaY"), 0);
                Instantiate(teleportAmatista, pos, Quaternion.identity);
            }
        }
        if (PlayerPrefs.HasKey("TeleportEsmeralda"))
        {
            if (PlayerPrefs.GetString("TeleportEsmeralda") == sceneName)
            {
                Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportEsmeraldaX"), PlayerPrefs.GetFloat("TeleportEsmeraldaY"), 0);
                Instantiate(teleportEsmeralda, pos, Quaternion.identity);
            }
        }
        if (PlayerPrefs.HasKey("TeleportOnixBlanco"))
        {
            if (PlayerPrefs.GetString("TeleportOnixBlanco") == sceneName)
            {
                Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportOnixBlancoX"), PlayerPrefs.GetFloat("TeleportOnixBlancoY"), 0);
                Instantiate(teleportOnixBlanco, pos, Quaternion.identity);
            }
        }
        if (PlayerPrefs.HasKey("TeleportJade"))
        {
            if (PlayerPrefs.GetString("TeleportJade") == sceneName)
            {
                Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportJadeX"), PlayerPrefs.GetFloat("TeleportJadeY"), 0);
                Instantiate(teleportJade, pos, Quaternion.identity);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
