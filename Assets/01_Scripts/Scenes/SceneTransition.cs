using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    public Vector3 newPosition;
    public string sceneLoad;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Player")
        {
            //GameManager.instance.playerData.lastPosition = newPosition;
            //GameManager.instance.playerData.lastLevel = sceneLoad;
            GameManager.instance.playerData.activeFlames = new List<int>();
            DoorManager.instance.indexFlames = 0;
            GameManager.instance.SaveGame(newPosition,sceneLoad);
            //GameSaveManager.instance.SaveGame();
            SceneManager.LoadScene(sceneLoad);
        }
    }
}
