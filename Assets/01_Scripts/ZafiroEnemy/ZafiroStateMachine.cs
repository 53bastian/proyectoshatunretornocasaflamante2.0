using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZafiroStateMachine : StateMachineBehaviour
{
    public IAZafiro zafiro;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        zafiro = animator.gameObject.GetComponent<IAZafiro>();
    }
}
