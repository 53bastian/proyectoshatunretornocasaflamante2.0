using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZafiroIdle : ZafiroStateMachine
{
    //public float timer = 0.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        //bossRoca.timer = 5;
        if (zafiro.timer <= 0)
        {
            zafiro.timer = 3;
        }
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (zafiro.distance <= 5)
        {
            animator.SetTrigger("meleeAttack");
            return;
        }
        else if (zafiro.distance <= 12)
        {
            animator.SetTrigger("rangeAttack");
            return;
        }
        if (zafiro.timer > 0)
        {
            zafiro.timer -= Time.deltaTime;
        }
        else
        {
            animator.SetBool("walking", true);
        }
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
