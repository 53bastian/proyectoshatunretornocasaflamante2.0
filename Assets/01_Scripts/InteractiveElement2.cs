using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveElement2 : InteractiveElement
{
    [Header("InteractiveElement2")]
    [Space]
    public Item_SO requiredStone;
    public TotemsPlatform totem;
    public GameObject stone;

    public override void Interac()
    {
        Debug.Log("Interac");
        if (InventorySystem.instance.SearchArtefact(requiredStone))
        {
            if (PlayerController.instance.UpdateManaPoints(-manaCost) && InventorySystem.instance.removeArtefact(requiredStone))
            {
                stone.SetActive(true);
                totem.UpdateStone();
                GetComponent<Collider2D>().enabled=false;
                manaCostText.text = "0";
            }
        }
    }

}
