﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Historia : MonoBehaviour
{
    public Dialogo dialogo;
    void Start()
    {
        //DesencaDialogo();
    }
    public void DesencaDialogo()
    {
        //FindObjectOfType<DialogoManager>().StartDialogo(dialogo);
        DialogoManager.instance.StartDialogo(dialogo);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            DesencaDialogo();
            gameObject.SetActive(false);
        }
    }

}
