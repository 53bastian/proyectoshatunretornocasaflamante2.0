﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogoManager : MonoBehaviour
{
    public Queue<Sentence> sentencia = new Queue<Sentence>();
    public Text nombretexto;
    public Text DialogoTexto;
    public GameObject PanelCharacters;
    public Image leftFace, rigthFace;
    public GameObject dialoguePanel;
    public static DialogoManager instance;
    public Dialogo d;
    public GameObject panelFinal;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            dialoguePanel.SetActive(false);
        }
    }

    // Start is called before the first frame update
    //void Start()
    //{
    //    //sentencia = new Queue<Sentence>();
    //}
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V) && dialoguePanel.activeSelf)
        {
            SiguienteOracion();
        }
    }
    public void StartDialogo(Dialogo dialogo)
    {
        // Debug.Log("Comienza la Conversacion con" + dialogo.nombre);

        //nombretexto.text = dialogo.nombre;
        //sentencia.Clear();
        d = dialogo;
        PlayerController.instance.StopPlayer();
        sentencia = new Queue<Sentence>();
        foreach (Sentence sentence in dialogo.sentencia)
        {
            sentencia.Enqueue(sentence);
            Debug.Log("Se agrego una sentencia");
        }
        Debug.Log(sentencia.Count);
        SiguienteOracion();
        dialoguePanel.SetActive(true);

    }

    public void SiguienteOracion()
    {
        Debug.Log("SiguienteOracion");
        if (sentencia.Count == 0)
        {
            EndDialogo();
            return;
        }
        leftFace.sprite = d.sentencia[d.index].leftFace;
        rigthFace.sprite = d.sentencia[d.index].rigthFace;
        Sentence sentence = sentencia.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
        Debug.Log(sentencia.Count);
        d.index++;
        //DialogoTexto.text = sentence;
        //Debug.Log(sentence);
    }
    IEnumerator TypeSentence(Sentence sentence)
    {
        //leftFace.sprite = sentence.leftFace;
        //rigthFace.sprite = sentence.rigthFace;
        nombretexto.text = sentence.nombre;
        DialogoTexto.text = "";
        foreach (char letter in sentence.sentencia.ToCharArray())
        {
            DialogoTexto.text += letter;
            yield return null;
        }
    }
    void EndDialogo()
    {
        Debug.Log("Termina la conversacion");
        PanelCharacters.SetActive(false);
        dialoguePanel.SetActive(false);
        if (d.finalDialogue)
        {
            panelFinal.SetActive(true);
        }
        else
        {
            PlayerController.instance.ResumPlayer();
        }
    }
    
    public void ReactiveLayer()
    {
        PlayerController.instance.ResumPlayer();
    }
}
