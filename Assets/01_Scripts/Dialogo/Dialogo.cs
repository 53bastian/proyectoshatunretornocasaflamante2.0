﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogo 
{
    //public string nombre;
    [HideInInspector] public int index = 0;
    public Sentence[] sentencia;
    public bool finalDialogue = false;
}
[System.Serializable]
public class Sentence
{
    public string nombre;
    [TextArea(3, 10)]
    public string sentencia;
    public Sprite leftFace;
    public Sprite rigthFace;
    
}