using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoDestructible : Destructible
{
    public List<ActivateWithPower> activateWithPowers;
    public Animator rubiAnim;
    public Animator onixAnim;

    public void checkPowers()
    {
        bool finish = true;
        foreach (var item in activateWithPowers)
        {
            if (!item.target.activeSelf)
            {
                if (item.power == PlayerController.currentPower)
                {
                    item.target.SetActive(true);
                    
                    if (item.power == 0)
                    {
                        onixAnim.SetTrigger("action");
                    }
                    else if (item.power==1)
                    {
                        rubiAnim.SetTrigger("action");
                    }



                }
                else
                {
                    finish = false;
                }
            }
        }
        if (finish)
        {
            anim.SetBool("action",true);
        }
    }

    public override void TakeDamage(float d)
    {
        //base.TakeDamage(d);
        checkPowers();
    }
}

[System.Serializable]
public class ActivateWithPower
{
    public int power;
    public GameObject target;
}