using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour, IDamageable
{
    public GameObject particlesPrefab;
    public Animator anim;
    public float life=3;
    public List<GameObject> itemsToSpawn = new List<GameObject>();
    public bool requireMana=false;
    //public float manaPower;
    public float manaCost;
    public TextMesh manaCostText;
    public TextMesh manaRequireText;
    // Start is called before the first frame update
    public virtual void Start()
    {
        //manaRequireText.text = manaPower.ToString();
        if (requireMana)
        {
            manaCostText.text = manaCost.ToString();
        }
    }

    public virtual void TakeDamage(float d)
    {
        if (requireMana)
        {
            if (PlayerController.instance.stats.currentMana < manaCost)
            {
                return;
            }
           
        }
        if (life - 1 > 0)
        {
            anim.SetTrigger("Herido");
        }
        life -= 1;
        if (life <= 0)
        {
            life = 0;
            StartCoroutine(Destroyer());
        }
    }

     public virtual IEnumerator Destroyer() {
        anim.SetTrigger("Destruido");
        Instantiate(particlesPrefab, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.2f);
        if (itemsToSpawn.Count>0)
        {
            for (int i = 0; i < itemsToSpawn.Count; i++)
            {
                Instantiate(itemsToSpawn[i], transform.position+new Vector3(Random.Range(-0.5f,0.5f),0,0), Quaternion.identity);
            }
        }
        if (requireMana)
            PlayerController.instance.UpdateManaPoints(-manaCost);
        Destroy(this.gameObject);
    }

  
}
