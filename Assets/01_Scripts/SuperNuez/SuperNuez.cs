using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperNuez : MonoBehaviour
{
    public int posNuez;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DoorManager.instance.UpdateNuez(posNuez);
            gameObject.SetActive(false);
            //Destroy(gameObject);
        }
    }
}
