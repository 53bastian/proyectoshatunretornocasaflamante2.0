using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public PlayerData_SO playerData;
    public Vector3 initialPosition;
    public string initialLevel;
    public static GameManager instance;
    

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            
        }
        else if (instance !=this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RestartValues()
    {
        playerData.lastPosition = initialPosition;
        playerData.lastLevel = initialLevel;
        playerData.mana = 0;
        playerData.superFlames = 0;
        playerData.superNuez = 0;
        for (int i = 0; i < playerData.keysPP.Count; i++)
        {
            if (PlayerPrefs.HasKey(playerData.keysPP[i]))
            {
                PlayerPrefs.DeleteKey(playerData.keysPP[i]);
            }
        }
        playerData.keysPP = new List<string>();
        playerData.activeFlames = null;
        playerData.activeNuez = null;
        playerData.itemsList = new List<ItemInfo>();
        playerData.itemsArtfct = new List<ItemInfo>();
        playerData.itemPowerUpList = new List<ItemInfo>();
    }
    public void SaveGame(Vector3 pos, string level)
    {
        playerData.lastPosition = pos;
        playerData.lastLevel = level;
        if (PlayerController.instance != null)
        {
            playerData.mana=PlayerController.instance.stats.currentMana;
            PlayerController.instance.Resetlife();
        }
        if (DoorManager.instance != null)
        {
            playerData.superFlames = DoorManager.instance.indexFlames;
        }
        for (int i = 0; i < playerData.keysPP.Count; i++)
        {
            if (!PlayerPrefs.HasKey(playerData.keysPP[i]))
            {
                PlayerPrefs.SetInt(playerData.keysPP[i], 1);
            }
        }

        GameSaveManager.instance.SaveGameSlot();
    }
    public void LoadGame()
    {
        //Time.timeScale = 1;
        GameSaveManager.instance.LoadGameSlot();
        
        SceneManager.LoadScene(playerData.lastLevel);
    }
    public void LoadGame(Vector3 newPos)
    {
        GameSaveManager.instance.LoadGameSlot();
        playerData.lastPosition = newPos;
        GameSaveManager.instance.SaveGameSlot();
        SceneManager.LoadScene(playerData.lastLevel);
    }
    
    public void AddItem(Item_SO item, int amount =0)
    {
        if (!playerData.SearchItem(item.itemName, amount))
        {
            ItemInfo newItem = new ItemInfo();
            newItem.itemName = item.itemName;
            newItem.itemQuantity = item.itemQuantity;
            newItem.itemCost = item.itemCost;
            newItem.itemSprite = item.itemSprite;
            newItem.action = item.action;
            newItem.canCombine = item.canCombine;
            playerData.itemsList.Add(newItem);
        }
    }
    public void AddItemArtefact(Item_SO item, int amount = 0)
    {
        if (!playerData.SearchItemArtefacts(item.itemName, amount))
        {
            ItemInfo newItem = new ItemInfo();
            newItem.itemName = item.itemName;
            newItem.itemQuantity = item.itemQuantity;
            newItem.itemCost = item.itemCost;
            newItem.itemSprite = item.itemSprite;
            newItem.action = item.action;
            playerData.itemsArtfct.Add(newItem);
        }
       
    }
    public void AddItemPowerUp(Item_SO item, int amount = 0)
    {
        if (!playerData.SearchItemPowerUp(item.itemName, amount))
        {
            ItemInfo newItem = new ItemInfo();
            newItem.itemName = item.itemName;
            newItem.itemQuantity = item.itemQuantity;
            newItem.itemCost = item.itemCost;
            newItem.itemSprite = item.itemSprite;
            newItem.action = item.action;
            playerData.itemPowerUpList.Add(newItem);
        }
    }
    
}
