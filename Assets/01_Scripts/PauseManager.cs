using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseManager : MonoBehaviour
{
    public GameObject pausePanel;
    public Transform startPoint;
    public GameObject deathPanel;
    public static PauseManager instance;
    public GameObject PanelMapa;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update

    void Start()
    {
        pausePanel.SetActive(false);
        deathPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UpdatePauseGame();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (PanelMapa.activeSelf)
            {
                PanelMapa.SetActive(false);
                Time.timeScale = 1;
            }
            else
            {
                PanelMapa.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }

    public void UpdatePauseGame()
    {
        if (pausePanel.activeSelf)
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
    }
    public void ReloadSavePoint()
    {
        Time.timeScale = 1;
        GameManager.instance.LoadGame();
    }
    public void RestartLevel()
    {
        Time.timeScale = 1;
        GameManager.instance.LoadGame(startPoint.position);
    }

    public void LoadMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }
    public void PlayerDeath()
    {
        //Time.timeScale = 0;
        deathPanel.SetActive(true);
    }
    public void PlayerResurec()
    {
        GameManager.instance.LoadGame();

    }
}
