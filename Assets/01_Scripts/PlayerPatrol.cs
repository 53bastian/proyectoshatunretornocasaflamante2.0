using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPatrol : MonoBehaviour
{
    PlayerController player;
    int index = 0;
    bool moving = false;
    public List<Transform> points;
    public float speed=2.5f;
    float speedMultipler = 1;

    private void Awake()
    {
        player = GetComponent<PlayerController>(); 
    }
 
    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            if (Input.GetKey(KeyCode.R))
            {
                speedMultipler = 2;
            }
            else
            {
                speedMultipler = 1;
            }
            transform.Translate(Vector2.right * speed * speedMultipler * Time.deltaTime);
            float d = Vector2.Distance(points[index].position, transform.position);
            if (d <= 0.5f)
            {
                index++;
                if (index < points.Count)
                {
                    RotateToPoint();
                }
                else
                {
                    player.state = PlayerState.normal;
                    player.rb2d.isKinematic = false;
                    player.anim.SetBool("flying", false);
                    moving = false;
                }
            }
        } 
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("PatrolStart"))
        {
            player.transform.localScale = new Vector3(1,1,1);
            player.state = PlayerState.frozen;
            player.rb2d.isKinematic = true;
            player.rb2d.velocity = Vector2.zero;
            player.anim.SetBool("flying", true);
            RotateToPoint();
            moving = true;
        }
    }
    void RotateToPoint()
    {
        Vector3 dir = points[index].position - transform.position;
        float angleZ = Mathf.Atan2(dir.x, dir.y)* Mathf.Rad2Deg-90;
        transform.rotation = Quaternion.Euler(0,0,-angleZ);
    } 
}
