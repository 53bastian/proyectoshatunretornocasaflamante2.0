using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/PlayerData")]
public class PlayerData_SO : ScriptableObject
{
    public Vector3 lastPosition;
    public string lastLevel;
    public float mana;
    public int superFlames;
    public int superNuez;
    public List<int> activeFlames;
    public List<int> activeNuez;
    public List<string> keysPP;
    public List<ItemInfo> itemsList;
    public List<ItemInfo> itemsArtfct;
    public List<ItemInfo> itemPowerUpList;

    public bool SearchItem(string itemName, int amount)
    {
        if (itemsList.Count > 0)
        {
            for (int i = 0; i < itemsList.Count; i++)
            {
                if (itemName == itemsList[i].itemName)
                {
                    Debug.Log("item: " + itemName + " " + amount );
                    itemsList[i].itemQuantity += amount;
                    Debug.Log("itemF : " + itemName + " " + itemsList[i].itemQuantity);
                    return true;
                }
            }
        }
        return false;
    }
    public bool SearchItemArtefacts(string itemName, int amount)
    {
        for (int i = 0; i < itemsArtfct.Count; i++)
        {
            if (itemName == itemsArtfct[i].itemName)
            {
                itemsArtfct[i].itemQuantity += amount;
                return true;
            }
        }
        return false;
    }
    public bool SearchItemPowerUp(string itemName, int amount)
    {
        for (int i = 0; i < itemPowerUpList.Count; i++)
        {
            if (itemName == itemPowerUpList[i].itemName)
            {
                itemPowerUpList[i].itemQuantity += amount;
                return true;
            }
        }
        return false;
    }
}

