using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightController2D : MonoBehaviour
{
    public Light2D point;
    public float speed= 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        point.pointLightInnerRadius = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (point.pointLightInnerRadius<=1f && speed<0)
        {
            speed *= -1;
        }
        else if (point.pointLightInnerRadius >= 2f && speed > 0)
        {
            speed *= -1;
        }
        point.pointLightInnerRadius += (speed * Time.deltaTime);
    }
}
