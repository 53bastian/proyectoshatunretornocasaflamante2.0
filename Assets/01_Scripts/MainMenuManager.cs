using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenuManager : MonoBehaviour
{
    public List<SaveSlot> slots;
    public int level;
    //public Text flamesSlot1;
    // Start is called before the first frame update
    void Start()
    {
        //ReStartGame();
        foreach (var slot in slots)
        {
            slot.StartValues(level);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void Exit()
    {
        Application.Quit();
    }

    public void ReStartGame()
    {
        PlayerPrefs.DeleteAll();
        GameManager.instance.RestartValues();
        GameSaveManager.instance.SaveGame();
    }
}
