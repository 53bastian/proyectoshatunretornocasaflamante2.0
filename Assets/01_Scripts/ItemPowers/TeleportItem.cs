using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportItem : MonoBehaviour
{
    public PlayerController player;
    public Transform target;
    public string levelToLoad;
    public Vector3 newPos;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Action();
            }
        }
    }

    public virtual void Action()
    {
        player.TeleportPower(target);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController p = collision.GetComponent<PlayerController>();
        if (p != null)
        {
            player = p;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerController p = collision.GetComponent<PlayerController>();
        if (p != null)
        {
            player = null;
        }
    }
}
