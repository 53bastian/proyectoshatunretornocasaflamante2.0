using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ConstructionItem : MonoBehaviour
{
    public GameObject platformPrefab;
    public Item_SO item;
    public bool canConstruction = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (EventSystem.current.currentSelectedGameObject == null)
        //{
        //    Debug.Log("null");
        //}
        //else
        //{
        //    Debug.Log(EventSystem.current.currentSelectedGameObject.name);
        //}
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButton(1))
        {
            Debug.Log("CancelConstruction");
            CancelConstruction();
        }
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 newPos = new Vector3(mousePos.x, mousePos.y, -1.063468f);
        transform.position = newPos;
        if (canConstruction && Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject==null)
        {
            StartCoroutine(MakeConstruction());
        }
    }

    public IEnumerator MakeConstruction()
    {
        canConstruction = false;
        yield return new WaitForSeconds(0.2f);
        Instantiate(platformPrefab, transform.position, Quaternion.identity);
        PlayerController.instance.constructionInProgress = false;
        Destroy(this.gameObject);
    }
    public void CancelConstruction()
    {
        InventorySystem.instance.AddNewItem(item);
        PlayerController.instance.constructionInProgress = false;
        Destroy(this.gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        canConstruction = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        canConstruction = false;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        canConstruction = true;

    }
}

