using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraTrigger : MonoBehaviour
{
    public float cameSize = 7;
    //float lastCameraSize=0;
    public CinemachineVirtualCamera cameraCM;

    void Start()
    {
        cameraCM = Camera.main.GetComponent<CinemachineVirtualCamera>();
        //lastCameraSize = cameraCM.m_Lens.OrthographicSize;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Player")
        {
            cameraCM.m_Lens.OrthographicSize= cameSize;
            //Destroy(gameObject);
        }
    }

}
