using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveElement : MonoBehaviour
{
    public bool loked=true;
    public bool canInteractive=false;
    public List<Animator> platformsObjcst;
    public Animator anim;
    public float manaCost = 40f;
    public TextMesh manaCostText;
    public bool requiereKey = false;
    public bool destroy=false;
    public Item_SO key;
    public bool savePoint= false;
    public string level;
    public bool isSuperFlame=false;
    public int posFlame;
    public GameObject flameLight;
    public Item_SO saveGameItem;
    public bool showAnim=false;
    public bool puzzleStone = false;
    public Item_SO itemPuzzleGem;
    public GameObject gemToActive;
    public Animator wallAnim;
    public Item_SO carbon;
    public int itemQuantityCost= 10;

    // Start is called before the first frame update
    void Start()
    {
        if (!requiereKey)
        {
            if (carbon== null)
            {
                manaCostText.text = manaCost.ToString();
            }
            else
            {
                manaCostText.text = itemQuantityCost.ToString();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Player")
        {
            Debug.Log("enotroaltiggir");
            canInteractive = true;
            PlayerController.instance.currentElement = this;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            canInteractive = false;
            PlayerController.instance.currentElement = null;
        }
    }

    public virtual void Interac() 
    {
        if (loked)
        {
            if (!requiereKey)
            {
                if (savePoint)
                {
                    if (InventorySystem.instance.RemoveItemPowerUp(saveGameItem)) //encuentra el item
                    {
                        canInteractive = false;
                        if (PlayerController.instance.transform.position.x < transform.position.x)
                        {
                            if (PlayerController.instance.transform.localScale.x < 0)
                            {
                                PlayerController.instance.transform.localScale = new Vector3(1, 1, 1);
                            }
                        }
                        else
                        {
                            if (PlayerController.instance.transform.localScale.x > 0)
                            {
                                PlayerController.instance.transform.localScale = new Vector3(-1, 1, 1);
                            }
                        }
                        GameManager.instance.SaveGame(transform.position, level); //Guardado la posicion del player
                        anim.SetBool("save", true);
                        Debug.Log("Guardo");
//                        GetComponent<Collider2D>().enabled = false;
                        PlayerController.instance.StartCoroutine(PlayerController.instance.SavePos());
                    }
                }else if(carbon != null)
                {
                    Debug.Log("InteractivePowerUp");
                    if (InventorySystem.instance.SearchPowerItemQuantity(carbon, itemQuantityCost))   
                    {
                        //PlayerController.instance.UpdateManaPoints(-manaCost);
                        for (int i = 0; i < itemQuantityCost; i++)
                        {
                            InventorySystem.instance.RemoveItemPowerUp(carbon);
                        }
                        PlayerController.instance.UnlockPower(2);
                    }
                }

                else if (PlayerController.instance.UpdateManaPoints(-manaCost))
                {
                    if (anim != null)
                    {
                        anim.speed = 1 + 1 * PlayerController.instance.stats.speedModifier; //velocidad de la palanca
                    }
                    manaCostText.text = "0";
                    loked = false;
                    if (!savePoint)
                    {
                        if (!isSuperFlame)
                        {
                            anim.SetBool("PlatformActive", true);
                            for (int i = 0; i < platformsObjcst.Count; i++)
                            {
                                platformsObjcst[i].speed= 1 + 1 * PlayerController.instance.stats.speedModifier; //velocidad de la palanca
                                platformsObjcst[i].gameObject.SetActive(true);
                                platformsObjcst[i].SetBool("PlatformActive", true);
                            }
                        }
                        else if(puzzleStone)
                        {
                            if (InventorySystem.instance.removeArtefact(itemPuzzleGem))
                            {
                                wallAnim.SetTrigger("action");
                                gemToActive.SetActive(true);
                            }
                        }
                        else
                        {
                            flameLight.SetActive(true);
                            anim.gameObject.SetActive(true);
                            DoorManager.instance.UpdateFlame(posFlame);
                            //gameObject.SetActive(false);
                        }
                        
                    }
                    GetComponent<Collider2D>().enabled = false;
                    PlayerController.instance.currentElement = null;
                }
            }
            else if(InventorySystem.instance.RemoveItem(key))
            {
                loked = false;
                anim.SetBool("UnLoked", true);
                PlayerController.instance.currentElement = null;
                if (destroy)
                {
                    StartCoroutine(Destroyer());
                }
            }
        }   
    }

    IEnumerator Destroyer()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }

    public void ActiveFlame()
    {
        manaCostText.text = "0";
        loked = false;
        flameLight.SetActive(true);
        anim.gameObject.SetActive(true);
       // DoorManager.instance.UpdateFlame(posFlame);
    }

}
