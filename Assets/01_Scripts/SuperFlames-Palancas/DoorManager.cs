using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorManager : MonoBehaviour
{
    public List<Animator> flames;
    public Animator door;
    public int indexFlames = 0;
    public int indexNuez = 0;
    public static DoorManager instance;
    public Text superFlames;

    public List<int> activeFlames= new List<int>();
    public List<GameObject> flamesList;
    public bool useNuez = false;
    public List<int> activeNuez = new List<int>();
    public List<SuperNuez> nuezList;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance==null)
        {
            instance = this;
            indexFlames = GameManager.instance.playerData.superFlames;
            indexNuez = GameManager.instance.playerData.superNuez;
            //for (int i = 0; i < flamesList.Count; i++)
            //{
            //    activeFlames.Add(0);
            //}
            if (GameManager.instance.playerData.activeFlames != null)
            {
                if (GameManager.instance.playerData.activeFlames.Count > 0)
                {
                    activeFlames = GameManager.instance.playerData.activeFlames;
                }
            }
            for (int i = 0; i < indexFlames; i++)
            {
                flames[i].SetTrigger("On");
                flames[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            }
            for (int i = 0; i < flames.Count; i++)
            {
                if (activeFlames[i] == 1)
                {
                    InteractiveElement ie = flamesList[i].GetComponent<InteractiveElement>();
                    if (ie != null)
                    {
                        ie.ActiveFlame();
                    }
                    else
                    {
                        flamesList[i].SetActive(false);
                    }
                }
            }
            superFlames.text = indexFlames.ToString();
            if (GameManager.instance.playerData.activeNuez != null)
            {
                if (GameManager.instance.playerData.activeNuez.Count > 0)
                {
                    activeNuez = GameManager.instance.playerData.activeNuez;
                }
            }
            for (int i = 0; i < nuezList.Count; i++)
            {
                if (activeNuez[i] == 1)
                {
                    nuezList[i].gameObject.SetActive(false);
                }
            }
            if (indexFlames >= flames.Count)
            {
                if (useNuez)
                {
                    if (indexNuez>=nuezList.Count)
                    {
                        CamaraController.instance.ShowAnotherObject(door.transform);
                        door.SetTrigger("On");
                    }
                }
                else
                {
                    CamaraController.instance.ShowAnotherObject(door.transform);
                    door.SetTrigger("On");
                }
            }
        }   
    }

    public void UpdateNuez(int n)
    {
        activeNuez[n] = 1;
        //flamesList[n].gameObject.SetActive(false);
        indexNuez++;
        GameManager.instance.playerData.superNuez = indexNuez;
        GameManager.instance.playerData.activeNuez = activeNuez;
        //superFlames.text = indexNuez.ToString();

        if (indexNuez >= nuezList.Count && indexFlames >=flames.Count)
        {
            CamaraController.instance.ShowAnotherObject(door.transform);
            door.SetTrigger("On");
        }
    }

    public void UpdateFlame(int n)
    {
        activeFlames[n] = 1;
        //flamesList[n].gameObject.SetActive(false);
        flames[indexFlames].SetTrigger("On");
        flames[indexFlames].GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
        indexFlames++;
        GameManager.instance.playerData.superFlames = indexFlames;
        GameManager.instance.playerData.activeFlames = activeFlames;
        superFlames.text = indexFlames.ToString();
       
        if (indexFlames >= flames.Count && indexNuez >= nuezList.Count)
        {
            CamaraController.instance.ShowAnotherObject(door.transform);
            door.SetTrigger("On");
        }
    }
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
