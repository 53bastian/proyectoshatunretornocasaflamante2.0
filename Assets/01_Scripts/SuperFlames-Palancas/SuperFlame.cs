using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperFlame : MonoBehaviour
{
    public int posFlame;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DoorManager.instance.UpdateFlame(posFlame);
            gameObject.SetActive(false);
            //Destroy(gameObject);
        }
    }
}
