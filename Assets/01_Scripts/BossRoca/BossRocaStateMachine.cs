using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRocaStateMachine : StateMachineBehaviour
{
    public IABossRoca bossRoca;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        bossRoca = animator.gameObject.GetComponent<IABossRoca>();
    }
}
