using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRocaLanzamientoRoca : BossRocaStateMachine
{
    public float timer = 0.0f;
    public int spawnedProyectiles = 0;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        timer = 0;
        spawnedProyectiles = 0;
        bossRoca.timer = 5;
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (PlayerController.instance.transform.position.x < bossRoca.transform.position.x)
        {
            animator.transform.localScale = new Vector3(-1, 1,1);

        }
        else
        {
            animator.transform.localScale = new Vector3(1, 1, 1);
        }
        if (timer < bossRoca.timeBtwAttack)
        {
            timer += Time.deltaTime;
            //tronco.Move();
        }
        else
        {
            //animator.SetBool("walking", false);
            bossRoca.SpawnProyectil();
            timer = 0;
            spawnedProyectiles++;
            if (spawnedProyectiles == bossRoca.proyectiles)
            {
                animator.SetTrigger("nextState");
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //tronco.rb.velocity = Vector2.zero;
        //animator.transform.localScale = new Vector3(animator.transform.localScale.x *(-1), 1,1);
        //tronco.lifeBar.transform.localScale  = new Vector3(animator.transform.localScale.x  , 1, 1);
        if (bossRoca.proyectiles < 5)
        {
            bossRoca.proyectiles++;
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
