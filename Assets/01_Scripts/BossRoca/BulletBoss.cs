using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBoss : MonoBehaviour
{
    public float damage;
    bool canDamage = true;
    public Rigidbody2D rb;
    public float bulletForce =2f;
    public float lifetime = 5f;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (lifetime > 0)
        {
            lifetime -= Time.deltaTime;
        }
        else
        {
            Destroyer();
        }
    }

    public void Init(float damage)
    {
        this.damage = damage;
        gameObject.SetActive(true);
        rb.AddForce(transform.up * bulletForce, ForceMode2D.Impulse);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (canDamage)
        {
            Debug.Log(collision.gameObject.name);
            PlayerController p = collision.GetComponent<PlayerController>();
            if (p != null)
            {
                canDamage = false;
                p.TakeDamage(damage);
                Destroyer();
            }
        }
    }


    void Destroyer()
    {
        Destroy(gameObject);
    }
}
