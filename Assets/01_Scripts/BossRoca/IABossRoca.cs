using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
public class IABossRoca : MonoBehaviour, IDamageable
{
    public float hp= 100;
    float maxHP;
    public float speed = 10;
    public Rigidbody2D rb;
    public float walkingTime = 5f;
    public Animator animxd;
    public DamageTriger damageTriger;
    public Image lifeBar;
    public List<GameObject> dropedItems;
    public SpriteRenderer sprite;
    public float distance = 0f;
    public float timeBtwAttack= 2f;
    public Transform firePoint;
    public BulletBoss bulletPrefab;
    public float damage=15;
    public int proyectiles = 3;
    public GameObject platformBoss;
    public bool canTakeDamage=false;
    public float timer = 0.0f;
    public Animator protectedDoor = null;


    public void Move()
    {
        rb.velocity = (transform.localScale.x > 0) ? Vector2.right*speed : Vector2.left*speed;
    }
    public void TakeDamage(float damage)
    {
        if (!canTakeDamage)
        {
            return;
        }
        if (hp == 0.0f)
        {
            return;
        }
        if (hp - damage > 0)
        {
            animxd.SetTrigger("Herido");
            //StartCoroutine(goRun());
        }
        hp -= damage;
        if (hp<=0)
        {
            hp = 0;
            Death();   
        }
        lifeBar.fillAmount = hp / maxHP;
    }

    void Death()
    {
        animxd.SetTrigger("Death");
        damageTriger.gameObject.SetActive(false);
        lifeBar.transform.parent.gameObject.SetActive(false);
        damageTriger.gameObject.SetActive(false);
        for (int i = 0; i < dropedItems.Count; i++)
        {
            Instantiate(dropedItems[i], transform.position + new Vector3(Random.Range(-0.5f, 0.5f), 0, 0),Quaternion.identity);
        }
        Camera.main.GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize=5;
        if (protectedDoor != null)
        {
            protectedDoor.SetTrigger("On");
        }
        if (WaveManager.instance != null)
        {
            WaveManager.instance.DeleteEnemy();
            StartCoroutine(DestroyEnemy());
        }
        
        //Destroy(this.gameObject);
        //StartCoroutine(ReStartNPC());
    }
    // Start is called before the first frame update
    void Start()
    {
        //animxd = GetComponent<Animator>();
        maxHP = hp;
        lifeBar.fillAmount = hp / maxHP;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 v1 = new Vector2(PlayerController.instance.transform.position.x, 0);
        Vector2 v2 = new Vector2(transform.position.x, 0);
        distance = Vector2.Distance(v1,v2);
    }
    IEnumerator goRun()
    {
        yield return new WaitForSeconds(1.5f);
        animxd.SetBool("walking", true);
    }
    IEnumerator ReStartNPC()
    {
        yield return new WaitForSeconds(2f);
        sprite.enabled = false;
        yield return new WaitForSeconds(28f);
        hp = maxHP;
        lifeBar.fillAmount = hp / maxHP;
        animxd.SetTrigger("Resurection");
        lifeBar.transform.parent.gameObject.SetActive(true);
        damageTriger.gameObject.SetActive(true);
        sprite.enabled = true;

    }
    public void SpawnProyectil()
    {
        Vector2 lookDir = PlayerController.instance.transform.position - firePoint.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        firePoint.eulerAngles = new Vector3(0, 0, angle);
        BulletBoss bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        bullet.Init(damage);
        Debug.Log("Disapro");
    }
    public void Onda()
    {
        StartCoroutine(IniciarOnda());
    }
    IEnumerator IniciarOnda()
    {
        platformBoss.SetActive(true);
        yield return new WaitForSeconds(5f);
        platformBoss.SetActive(false);
        animxd.SetTrigger("nextState");
    }
    IEnumerator DestroyEnemy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }
}
