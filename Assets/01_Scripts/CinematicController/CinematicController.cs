using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicController : MonoBehaviour
{
    public PlayerController player;
    public Animator blackCinematic;
    public GameObject cinematicCarbon;
    public GameObject mCamera;
    public void StartCinematic()
    {
        player.StopPlayer();
        blackCinematic.SetBool("open", true);
    }

    public void ContinueCinematic()
    {
        mCamera.SetActive(false);
        player.gameObject.SetActive(false);
        
        cinematicCarbon.SetActive(true);
    }
}
