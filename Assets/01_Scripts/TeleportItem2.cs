using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportItem2 : TeleportItem
{
    public bool staticTotem = false; 
    public TotemTeleportType type;
    public Animator anim;
    public LayerMask playerMask;
    public List<GameObject> gems;

    void Start()
    {
        if (staticTotem)
        {
            Debug.Log("entroTotem");
            StartCoroutine(SearchPlayer());
        }
        else
        {
            CheckTotems();
        }
    }
    public void CheckTotems()
    {
        switch (type)
        {
            case TotemTeleportType.totemAmatista:
                if (PlayerPrefs.HasKey("TeleportAmatista"))
                {
                    string sceneN = PlayerPrefs.GetString("TeleportAmatista");
                    if (SceneManager.GetActiveScene().name==sceneN)
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportAmatistaX"), PlayerPrefs.GetFloat("TeleportAmatistaY"), 0);
                        transform.position = pos;
                    }
                }
                break;
            case TotemTeleportType.totemTurquesa:
                if (PlayerPrefs.HasKey("TeleportTurquesa"))
                {
                    string sceneN = PlayerPrefs.GetString("TeleportTurquesa");
                    if (SceneManager.GetActiveScene().name == sceneN)
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportTurquesaX"), PlayerPrefs.GetFloat("TeleportTurquesaY"), 0);
                        transform.position = pos;
                    }
                }
                break;
            case TotemTeleportType.totemEsmeralda:
                if (PlayerPrefs.HasKey("TeleportEsmeralda"))
                {
                    string sceneN = PlayerPrefs.GetString("TeleportEsmeralda");
                    if (SceneManager.GetActiveScene().name == sceneN)
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportEsmeraldaX"), PlayerPrefs.GetFloat("TeleportEsmeraldaY"), 0);

                        transform.position = pos;
                    }
                }
                break;
            case TotemTeleportType.totemOnixBlanco:
                if (PlayerPrefs.HasKey("TeleportOnixBlanco"))
                {
                    string sceneN = PlayerPrefs.GetString("TeleportOnixBlanco");
                    if (SceneManager.GetActiveScene().name == sceneN)
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportOnixBlancoX"), PlayerPrefs.GetFloat("TeleportOnixBlancoY"), 0);
                        transform.position = pos;
                    }
                }
                break;
            case TotemTeleportType.totemAmbar:
                if (PlayerPrefs.HasKey("TeleportAmbar"))
                {
                    string sceneN = PlayerPrefs.GetString("TeleportAmbar");
                    if (SceneManager.GetActiveScene().name == sceneN)
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportAmbarX"), PlayerPrefs.GetFloat("TeleportAmbarY"), 0);
                        transform.position = pos;
                    }
                }
                break;
            case TotemTeleportType.totemJade:
                if (PlayerPrefs.HasKey("TeleportJade"))
                {
                    string sceneN = PlayerPrefs.GetString("TeleportJade");
                    if (SceneManager.GetActiveScene().name == sceneN)
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportJadeX"), PlayerPrefs.GetFloat("TeleportJadeY"), 0);
                        transform.position = pos;
                    }
                }
                break;
            default:
                break;
        }
    }
    public override void Action()
    {
       
        if (!staticTotem)
        {
            PlayerController.instance.TeleportPower2(newPos,levelToLoad);
        }
        else
        {
            switch (type)
            {
                case TotemTeleportType.totemAmatista:
                    if (PlayerPrefs.HasKey("TeleportAmatista"))
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportAmatistaX"), PlayerPrefs.GetFloat("TeleportAmatistaY"), 0);
                        PlayerController.instance.TeleportPower2(pos, PlayerPrefs.GetString("TeleportAmatista"));
                    }
                    break;
                case TotemTeleportType.totemTurquesa:
                    if (PlayerPrefs.HasKey("TeleportTurquesa"))
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportTurquesaX"), PlayerPrefs.GetFloat("TeleportTurquesaY"), 0);
                        PlayerController.instance.TeleportPower2(pos, PlayerPrefs.GetString("TeleportTurquesa"));
                    }
                    break;
                case TotemTeleportType.totemEsmeralda:
                    if (PlayerPrefs.HasKey("TeleportEsmeralda"))
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportEsmeraldaX"), PlayerPrefs.GetFloat("TeleportEsmeraldaY"), 0);
                        PlayerController.instance.TeleportPower2(pos, PlayerPrefs.GetString("TeleportEsmeralda"));
                    }
                    break;
                case TotemTeleportType.totemOnixBlanco:
                    if (PlayerPrefs.HasKey("TeleportOnixBlanco"))
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportOnixBlancoX"), PlayerPrefs.GetFloat("TeleportOnixBlancoY"), 0);
                        PlayerController.instance.TeleportPower2(pos, PlayerPrefs.GetString("TeleportOnixBlanco"));
                    }
                    break;
                case TotemTeleportType.totemAmbar:
                    if (PlayerPrefs.HasKey("TeleportAmbar"))
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportAmbarX"), PlayerPrefs.GetFloat("TeleportAmbarY"), 0);
                        PlayerController.instance.TeleportPower2(pos, PlayerPrefs.GetString("TeleportAmbar"));
                    }
                    break;
                case TotemTeleportType.totemJade:
                    if (PlayerPrefs.HasKey("TeleportJade"))
                    {
                        Vector3 pos = new Vector3(PlayerPrefs.GetFloat("TeleportJadeX"), PlayerPrefs.GetFloat("TeleportJadeY"), 0);
                        PlayerController.instance.TeleportPower2(pos, PlayerPrefs.GetString("TeleportJade"));
                    }
                    break;
                default:
                    break;
            }
        }
    }
    IEnumerator SearchPlayer()
    {
        yield return new WaitForSeconds(0.5f);
        Collider2D[] player = Physics2D.OverlapBoxAll(transform.position, new Vector2(10, 10), 0, playerMask);
        if (player.Length > 0)
        {
            foreach (var gem in gems)
            {
                gem.SetActive(true);
            }
            Debug.Log("encontrototem");
            anim.SetBool("action", true);
        }
    }
}
public enum TotemTeleportType
{
    totemAmatista,
    totemTurquesa,
    totemEsmeralda,
    totemOnixBlanco,
    totemAmbar,
    totemJade
}