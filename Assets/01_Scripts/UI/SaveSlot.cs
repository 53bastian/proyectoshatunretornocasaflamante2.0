using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class SaveSlot : MonoBehaviour
{
    public GameObject newGamePanel;
    public GameObject continueGamePanel;
    public int slot = 0;
    public Text txtMana;
    public Text txtFlames;
    public Text txtLevel;
    public Text txtScene;
    int level=1;
    // Start is called before the first frame update
    public void StartValues( int newLevel)
    {
        level = newLevel;
        if (PlayerPrefs.HasKey("GameSlot" + slot))
        {
            newGamePanel.SetActive(false);
            continueGamePanel.SetActive(true);
            GameSaveManager.instance.LoadGameSlot(slot);
            txtMana.text = (GameManager.instance.playerData.mana).ToString();
            txtFlames.text = (GameManager.instance.playerData.superFlames).ToString();
            txtLevel.text = GameManager.instance.playerData.lastLevel;
            txtScene.text = GetCurrentScene(GameManager.instance.playerData.lastLevel);
        }
        else
        {
            newGamePanel.SetActive(true);
            continueGamePanel.SetActive(false);
        }
    }
    string GetCurrentScene(string lastScene)
    {
        if (lastScene == "Nivel-1" || lastScene == "Nivel-2")
        {
            return "Bosque";
        }
        else if (lastScene == "Nivel-3" || lastScene == "Nivel-4")
        {
            return "Cueva";
        }
        return "Atardecer";
    }
 

    public void NewGameSlot()
    {
        PlayerPrefs.SetInt("GameSlot" + slot, 1);
        GameManager.instance.RestartValues();
        GameManager.instance.playerData.lastLevel = "Level-" + level;
        GameSaveManager.instance.SaveGameSlot(slot);
        SceneManager.LoadScene(GameManager.instance.playerData.lastLevel);
       // SceneManager.LoadScene("Nivel-" +level);

    }
    public void LoadGameSlot()
    {
        GameSaveManager.instance.LoadGameSlot(slot);
        SceneManager.LoadScene(GameManager.instance.playerData.lastLevel);
    }

    public void DeleteSlot()
    {
        PlayerPrefs.DeleteKey("GameSlot" + slot);
        GameManager.instance.RestartValues();
        GameSaveManager.instance.SaveGameSlot(slot);
        newGamePanel.SetActive(true);
        continueGamePanel.SetActive(false);
    }
}
