using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible2 : MonoBehaviour
{
    public Animator anim;
    public Collider2D collider;
    public void Destroyer()
    {
        StartCoroutine(DestroyerIE());
    }

    IEnumerator DestroyerIE()
    {
        collider.enabled = false;
        anim.SetTrigger("action");
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
}
