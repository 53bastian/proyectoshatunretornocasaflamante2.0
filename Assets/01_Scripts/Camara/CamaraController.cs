using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CamaraController : MonoBehaviour
{
    public Transform target;
    //public Vector3 ofset;
    public CinemachineVirtualCamera cameraCM;
    public static CamaraController instance;

    private void Awake()
    {
        if (instance==null)
        {
            instance = this;
            cameraCM.Follow = target;
        }
    }
    public void ShowAnotherObject(Transform obj)
    {
        StartCoroutine(ShowObj(obj));
    }

    IEnumerator ShowObj(Transform obj)
    {
        cameraCM.GetCinemachineComponent<CinemachineTransposer>().m_YDamping = 1;
        cameraCM.Follow = obj;
        yield return new WaitForSeconds(3f);
        cameraCM.GetCinemachineComponent<CinemachineTransposer>().m_YDamping = 0;
        cameraCM.Follow = target;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = target.position + ofset;
    }
}
