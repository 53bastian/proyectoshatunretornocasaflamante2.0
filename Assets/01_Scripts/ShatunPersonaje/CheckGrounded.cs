using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGrounded : MonoBehaviour
{
    private PlayerController player;
    public LayerMask groundLayer;
    public Transform pointer;
    public float checkRadius= 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<PlayerController>();
    }
    void Update() {
        if (player.state == PlayerState.stairs)
        {
            RaycastHit2D hit = Physics2D.Raycast(pointer.position, Vector2.up * (-1f), 0.01f, groundLayer);
            Debug.DrawRay(pointer.position, Vector2.up * (-1f) * 0.01f, Color.red);
            if (hit)
            {
                //Debug.Log("ground");
                player.grounded = true;
                player.anim.SetBool("Grounded",true);
                if (player.state == PlayerState.stairs)
                {
                    player.state = PlayerState.normal;
                }
            }
        }
        else
        {
            Collider2D GO= Physics2D.OverlapBox(pointer.position, new Vector2(checkRadius, 0.1f), groundLayer);
            bool grounded = false;
            if (GO!=null)
            {
                if (GO.gameObject.tag=="Ground")
                {
                    grounded = true;
                }
            }
            if (grounded)
            {
                //Debug.Log(GO.gameObject.name);
                //Debug.Log("ground");
                if (player.state == PlayerState.stairs)
                {
                    player.state = PlayerState.normal;
                }
            }
            player.grounded = grounded;
        } 
       
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(pointer.position, new Vector2(checkRadius, 0.1f));
    }

    //void OnCollisionEnter2D(Collision2D col)
    //{
    //    if (col.gameObject.tag =="Ground")
    //    {
    //        Debug.Log("ground");
    //        player.grounded = true;
    //        if (player.state==PlayerState.stairs)
    //        {
    //            player.state = PlayerState.normal;
    //        }
    //    }
    //}

    //void OnCollisionExit2D(Collision2D col)
    //{
    //    if (col.gameObject.tag == "Ground")
    //    {
    //        player.grounded = false;
    //    }
    //}
}
