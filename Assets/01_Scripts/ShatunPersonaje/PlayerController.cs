using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PowerInfo
{
    public string powerName;
    public int powerCode;
    public bool activated =false;
}


public class PlayerController : MonoBehaviour {
    public Transform attackPos;
    public Vector3 attackRange;
    public PlayerStats_SO stats; //stats
    float currentTimeBTWAttack = 0;
    bool canAttack = true;
    public Rigidbody2D rb2d;
    [Range(1, 3)] public int level = 1;
    public List<Animator> levelAnimator;
    public Animator anim;
    public bool grounded;
    public PlayerState state = PlayerState.normal;
    public bool nearStair = false;
    public Transform pointer;
    public LayerMask groundLayer;
    public LayerMask stairsLayer;
    public LayerMask enemysLayer;
    bool takingDmg = false;
    public Image lifeBar;
    public Image manaBar;
    public Text lifeVidaText;
    public Text manaText;
    //contadorllaves
    public Text keystext;
    public static PlayerController instance;
    public InteractiveElement currentElement;
    public bool canMove = true;
    bool canTakeDamage = true;
    float currentTimeBTWTakeDamage = 0f;
    SpriteRenderer sprite;
    public Animator manabackground;
    //public List<PowerInfo> powers;
    public static int currentPower = -1;

    public Text E1txt, E2txt, E3txt;
    //ItemsPowers
    public List<Transform> teleportTargets;
    public int teleportIndex = 0;
    public GameObject teleportPrefab;

    public GameObject teleportTurquesa;
    public GameObject teleportAmatista;
    public GameObject teleportOnixBlanco;
    public GameObject teleportAmbar;
    public GameObject teleportEsmeralda;
    public GameObject teleportJade;

    public GameObject contructionPrefab;
    public bool constructionInProgress = false;

    //Magic
    public float magicTimer = 0;
    public bool magicActive = false;
    public string nivelScene;

    //ataqueRubi
    //public bool 
    public void InteractivePalanca()
    {
        if (currentElement != null)
        {
            
            currentElement.Interac();
            //StartCoroutine(SavePos());
        }
    }
    void Awake()
    {

        if (instance == null)
        {
            instance = this;
            rb2d = GetComponent<Rigidbody2D>();
            anim = Instantiate(levelAnimator[level - 1], transform.position, Quaternion.identity, transform);
            sprite = anim.GetComponent<SpriteRenderer>();
            if (stats.life <= 0)
            {
                stats.life = stats.maxHp;
            }
            if (GameManager.instance != null)
            {
                transform.position = GameManager.instance.playerData.lastPosition;
                stats.currentMana = GameManager.instance.playerData.mana;
            }
        }
    }
    void Start() {
        stats.maxHp = stats.life;
        lifeBar.fillAmount = stats.life / stats.maxHp;
        manaBar.fillAmount = stats.currentMana / stats.maxMana;
        lifeVidaText.text = stats.life.ToString();
        manaText.text = stats.currentMana.ToString();
        manabackground.SetBool("empty", stats.currentMana <= 0 ? true : false);
        E1txt.text = stats.e1.ToString();
        E2txt.text = stats.e2.ToString();
        E3txt.text = stats.e3.ToString();
    }

    public bool CheckPower(int code) {

        if (currentPower == code)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void UnlockPower(int code)
    {
        currentPower = code;
        anim.SetInteger("attackN", currentPower);
        Debug.Log("PoderActual " + code);
    }

    void Update() {
        if (stats.life <= 0 || !canMove)
        {
            return;
        }
        //Magic
        if (magicActive)
        {
            if (magicTimer > 0)
            {
                magicTimer -= Time.deltaTime;
            }
            else
            {
                magicActive = false;
                anim.speed = 1;
                stats.speedModifier = 0;
            }
        }
        Jump();
        anim.SetFloat("speed", Mathf.Abs(rb2d.velocity.x));
        anim.SetBool("Grounded", grounded);
        if (Input.GetKeyDown(KeyCode.R) && state != PlayerState.stairs && canAttack)
        {
            if (CheckPower(0)) //mandar codigo del ataque
            {
                MakeAttack();
                Debug.Log("AtaquePoder1");
            }
            else if (CheckPower(1)) //mandar codigo del ataque
            {
                MakeAttackPowerRubi();
                Debug.Log("AtaquePoder2");
            }
            else if (CheckPower(2) && state != PlayerState.flying) //mandar codigo del ataque
            {
                rb2d.gravityScale = 0;
                state = PlayerState.flying;
                anim.SetBool("flying", true);
                rb2d.velocity = new Vector2((transform.localScale.x > 0 ? 1 : -1) * (stats.SpeedMax * 2f), 0);
                //rb2d.AddForce(new Vector2(0,5), ForceMode2D.Impulse);
                transform.position += transform.up * 0.5f;
                Debug.Log("AtaquePoder3");
            }
        }
        if (!canAttack)
        {
            if (currentTimeBTWAttack < stats.TimeBTWAttack)
            {
                currentTimeBTWAttack += Time.deltaTime;
            }
            else
            {
                canAttack = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            InteractivePalanca();
        }
        if (!canTakeDamage)
        {
            if (currentTimeBTWTakeDamage < stats.timeBTWTakeDamage)
            {
                currentTimeBTWTakeDamage += Time.deltaTime;
            }
            else
            {
                currentTimeBTWTakeDamage = 0;
                canTakeDamage = true;
                sprite.color = new Color(1, 1, 1, 1);
            }
        }

    }

    public bool UpdateManaPoints(float points)
    {

        if (stats.currentMana + points >= 0 && stats.currentMana + points <= stats.maxMana)
        {
            stats.currentMana += points;
            Debug.Log("Cost" + points);
            manaBar.fillAmount = stats.currentMana / stats.maxMana;
            manaText.text = stats.currentMana.ToString();
            manabackground.SetBool("empty", stats.currentMana <= 0 ? true : false);
            return true;
        }
        return false;
    }

    public bool UpdateEnergys(int energyA, int energyB)
    {

        switch (energyA)
        {
            case 0:
                if (stats.e1 - 1 < 0)
                {
                    return false;
                }
                break;
            case 1:
                if (stats.e2 - 1 < 0)
                {
                    return false;
                }
                break;
            case 2:
                if (stats.e3 - 1 < 0)
                {
                    return false;
                }
                break;
        }
        switch (energyB)
        {
            case 0:
                if (stats.e1 - 1 < 0)
                {
                    return false;
                }
                break;
            case 1:
                if (stats.e2 - 1 < 0)
                {
                    return false;
                }
                break;
            case 2:
                if (stats.e3 - 1 < 0)
                {
                    return false;
                }
                break;
        }
        switch (energyA)
        {
            case 0:
                stats.e1 -= 1;
                break;
            case 1:
                stats.e2 -= 1;
                break;
            case 2:
                stats.e3 -= 1;
                break;
        }
        switch (energyB)
        {
            case 0:
                stats.e1 -= 1;
                break;
            case 1:
                stats.e2 -= 1;
                break;
            case 2:
                stats.e3 -= 1;
                break;
        }
        E1txt.text = stats.e1.ToString();
        E2txt.text = stats.e2.ToString();
        E3txt.text = stats.e3.ToString();
        return true;
    }
    public void AddEnergy(int e)
    {
        if (e == 0)
        {
            stats.e1++;
            E1txt.text = stats.e1.ToString();
        }
        else if (e == 1) {
            stats.e2++;
            E2txt.text = stats.e2.ToString();
        }
        else if (e == 2)
        {
            stats.e3++;
            E3txt.text = stats.e3.ToString();
        }
    }

    public bool UpdateKeysPoints(float points)
    {
        if (stats.keys + points >= 0)
        {
            stats.keys += (int)points;

            keystext.text = stats.keys.ToString();
            return true;
        }
        return false;
    }
    void MakeAttack() {
        anim.speed = 1 + 1 * stats.speedModifier; //velocida de ataque
        canAttack = false;
        currentTimeBTWAttack = 0;
        anim.SetTrigger("OnAttack");
        Collider2D[] enemysToDamage = Physics2D.OverlapBoxAll(attackPos.position, attackRange, 0, enemysLayer);
        for (int i = 0; i < enemysToDamage.Length; i++)
        {
            IDamageable damageable = enemysToDamage[i].GetComponent<IDamageable>();
            if (damageable != null)
            {
                damageable.TakeDamage(stats.damage);
                Debug.Log("Encontroenemigo");
            }
        }
    }

    void MakeAttackPowerRubi()
    {
        anim.speed = 1 + 1 * stats.speedModifier; //velocida de ataque
        canAttack = false;
        currentTimeBTWAttack = 0;
        anim.SetTrigger("OnAttack");
        Collider2D[] enemysToDamage = Physics2D.OverlapBoxAll(attackPos.position, attackRange, 0, enemysLayer);
        for (int i = 0; i < enemysToDamage.Length; i++)
        {
            IDamageable damageable = enemysToDamage[i].GetComponent<IDamageable>();
            if (damageable != null)
            {
                damageable.TakeDamage(stats.damage * 2f);
            }
        }
    }
    public void TakeDamage(float d)
    {
        if (stats.life == 0 || !canTakeDamage)
        {
            return;
        }
        if (stats.life - d > 0)
        {
            anim.SetTrigger("Herido");
            if (!takingDmg)
            {
                StartCoroutine(TakingDMG());
                canTakeDamage = false;
                sprite.color = new Color(1, 1, 1, 0.8f);

            }
        }
        stats.life -= d;

        if (stats.life <= 0)
        {
            Death();
        }
        lifeBar.fillAmount = stats.life / stats.maxHp;
        lifeVidaText.text = stats.life.ToString();
    }
    public void Death()
    {
        PauseManager.instance.PlayerDeath();
        stats.life = 0;
        Debug.Log("Playerdath");
        anim.SetTrigger("Death");
        rb2d.velocity = Vector2.zero;
        if (WaveManager.instance != null)
        {
            WaveManager.instance.EndGame(false);
        }
    }
    public void AddLife(float value)
    {
        stats.life += value;
        if (stats.life > stats.maxHp)
        {
            stats.life = stats.maxHp;
        }
        lifeBar.fillAmount = stats.life / stats.maxHp;
        lifeVidaText.text = stats.life.ToString();
    }


    void Jump() {
        if (state == PlayerState.normal) {
            if (Input.GetKeyDown(KeyCode.Space) && grounded) {
                rb2d.AddForce(Vector2.up * stats.jumpForce, ForceMode2D.Impulse);
            }
        }
        else if (state == PlayerState.stairs) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                rb2d.isKinematic = false;
                state = PlayerState.normal;
                anim.speed = 1;
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(Vector2.up * stats.jumpForce, ForceMode2D.Impulse);
            }
        }
    }

    void FixedUpdate() {
        if (stats.life <= 0 || !canMove)
        {
            return;
        }
        if (!takingDmg && canMove)
        {
            Run();
        }
        Stairs();
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "stairs") {
            nearStair = true;
        }
        else
        {
            if (state == PlayerState.flying) //mandar codigo del ataque
            {
                Destructible2 d = collision.gameObject.GetComponent<Destructible2>();
                if (d != null)
                {
                    Debug.Log("entro al trigger");
                    d.Destroyer();
                }
            }
        }
    }
    void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "stairs")
        {
            nearStair = false;
        }
    }

    void Stairs() {
        if (state == PlayerState.flying)
        {
            return;
        }
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");
        if (v != 0 || h != 0) {
            anim.speed = 1;
            if (state == PlayerState.stairs) {
                if (!nearStair) {
                    state = PlayerState.normal;
                    return;
                }
                if (!grounded || (grounded && v > 0))
                {
                    rb2d.isKinematic = true;
                    rb2d.velocity = Vector2.zero;
                    float limitedSpeed = Mathf.Clamp(stats.Speed * v, -stats.SpeedMax, stats.SpeedMax);
                    float limitedSpeedH = Mathf.Clamp(stats.Speed * h, -stats.SpeedMax, stats.SpeedMax);
                    rb2d.transform.Translate(limitedSpeedH * Time.deltaTime, limitedSpeed * Time.deltaTime, 0);
                }
            }
            else if (state == PlayerState.normal && nearStair)
            {
                if (v < 0) {
                    RaycastHit2D hit = Physics2D.Raycast(pointer.position, Vector2.up * (-1f), 0.1f, groundLayer);
                    Debug.DrawRay(pointer.position, Vector2.up * (-1f) * 0.1f, Color.red);
                    if (!hit) {
                        state = PlayerState.stairs;
                        transform.position = new Vector3(transform.position.x, transform.position.y - 2.0f, transform.position.z);
                        grounded = false;
                    }
                } else if (v != 0) {


                    if (Physics2D.OverlapCircle(pointer.position + new Vector3(0f, 0.3f, 0f), 0.1f, stairsLayer))
                    {
                        state = PlayerState.stairs;
                        grounded = false;
                        rb2d.isKinematic = true;
                        rb2d.velocity = Vector2.zero;
                        float limitedSpeed = Mathf.Clamp(stats.Speed * v, -stats.SpeedMax, stats.SpeedMax);
                        float limitedSpeedH = Mathf.Clamp(stats.Speed * h, -stats.SpeedMax, stats.SpeedMax);
                        rb2d.transform.Translate(limitedSpeedH * Time.deltaTime, limitedSpeed * Time.deltaTime, 0);
                    }
                }
            }
        }
        else
        {
            if (state == PlayerState.stairs)
            {
                anim.speed = 0;
            }
        }
        if (state == PlayerState.stairs)
        {
            anim.SetBool("OnStairs", true);
        }
        else
        {
            anim.SetBool("OnStairs", false);
        }
    }

    public void StopPlayer()
    {
        canMove = false;
        //rb2d.gravityScale = 0;
        rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        anim.SetFloat("speed", 0);
        anim.SetBool("Grounded", true);
    }

    public void ResumPlayer()
    {
        //rb2d.gravityScale = 1;
        //rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        canMove = true;
    }
    void Run() {
        if (state == PlayerState.normal) {
            rb2d.isKinematic = false;
            float h = Input.GetAxisRaw("Horizontal");
            //Debug.Log(h);
            float limitedSpeed = Mathf.Clamp(stats.Speed * h, -stats.SpeedMax, stats.SpeedMax);
            rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);
            if (h > 0.1f)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
            if (h < -0.1f)
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }
        else if (state == PlayerState.flying)
        {
            rb2d.isKinematic = false;
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            float limitedSpeedH = Mathf.Clamp(stats.Speed * h, -stats.SpeedMax, stats.SpeedMax) * 2f;
            float limitedSpeedV = Mathf.Clamp(stats.Speed * v, -stats.SpeedMax, stats.SpeedMax) * 2f;
            float defaultSpeedH = (rb2d.velocity.x > 0 ? 1 : -1) * (stats.SpeedMax * 2f);
            float defaultSpeedV = (rb2d.velocity.y > 0 ? 1 : -1) * (stats.SpeedMax * 2f);
            //rb2d.velocity = new Vector2((limitedSpeedH != 0) ? limitedSpeedH : defaultSpeedH, (limitedSpeedV !=0) ? limitedSpeedV : defaultSpeedV);
            if (h != 0 || v != 0)
            {
                if (h != 0)
                {
                    rb2d.velocity = new Vector2(limitedSpeedH, 0);
                }
                else if (v != 0)
                {
                    rb2d.velocity = new Vector2(0, limitedSpeedV);
                }
                float angle = 0f;
                if (v != 0)
                {
                    if (v > 0)
                    {
                        angle = 90f;
                    }
                    else if (v < 0)
                    {
                        angle = -90f;
                    }
                }
                anim.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));
            }
            else
            {
                //rb2d.velocity = new Vector2(defaultSpeedH, defaultSpeedV);
                if (rb2d.velocity.x == 0)
                {
                    rb2d.velocity = new Vector2(0, defaultSpeedV);
                }
                else if (rb2d.velocity.y == 0)
                {
                    rb2d.velocity = new Vector2(defaultSpeedH, 0);
                }
            }
            if (h > 0.1f)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
            if (h < -0.1f)
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }

        }
    }

    public void Resetlife()
    {
        stats.life = stats.maxHp;
        lifeBar.fillAmount = 1;
        lifeVidaText.text = stats.life.ToString();
    }
    IEnumerator TakingDMG()
    {
        takingDmg = true;
        rb2d.velocity = Vector2.zero;
        rb2d.AddForce(new Vector2((transform.localScale.x > 0 ? -1 : 1), 1) * stats.jumpForce / 2, ForceMode2D.Impulse);
        yield return new WaitForSeconds(0.3f);
        takingDmg = false;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(attackPos.position, attackRange);
    }

    public void ActivateItem(Item_SO item) {
        switch (item.action)
        {
            case "Teleport":
                if (InventorySystem.instance.RemoveItem(item))
                {
                    SpawmTeleportItem();
                }
                break;
            case "ContructionPlatform":
                if (!constructionInProgress)
                {
                    if (InventorySystem.instance.RemoveItem(item))
                    {
                        SpawmPlatformConstruction();
                    }
                }
                break;
            case "Magic":
                if (!magicActive)
                {
                    if (InventorySystem.instance.RemoveItem(item))
                    {
                        stats.speedModifier = 1.0f;
                        anim.speed = 1 + 1 * stats.speedModifier;
                        magicTimer = 10;
                        magicActive = true;
                    }
                }
                break;
            case "TeleportAmatista":
                if (InventorySystem.instance.RemoveItem(item))
                {
                    SpawmTeleportAmatista();
                }
                break;
            case "TeleportTusquesa":
                if (InventorySystem.instance.RemoveItem(item))
                {
                    SpawmTeleportTurquesa();
                }
                break;
            case "TeleportEsmeralda":
                if (InventorySystem.instance.RemoveItem(item))
                {
                    SpawmTeleportEsmeralda();
                }
                break;
            case "TeleportOnixBlanco":
                if (InventorySystem.instance.RemoveItem(item))
                {
                    SpawmTeleportOnixBlanco();
                }
                break;
            case "TeleportAmbar":
                if (InventorySystem.instance.RemoveItem(item))
                {
                    SpawmTeleportAmbar();
                }
                break;
            case "TeleportJade":
                if (InventorySystem.instance.RemoveItem(item))
                {
                    SpawmTeleportJade();
                }
                break;
        }
    }
    public void SpawmTeleportItem()
    {
        GameObject go = Instantiate(teleportPrefab, attackPos.position, Quaternion.identity);
        TeleportItem ti = go.GetComponentInChildren<TeleportItem>();
        ti.target = teleportTargets[teleportIndex];
        if (teleportIndex < teleportTargets.Count - 1)
        {
            teleportIndex++;
        }
        Debug.Log("CreateTeleport");
    }

    public void SpawmTeleportAmatista()
    {
        GameObject go = Instantiate(teleportAmatista, attackPos.position, Quaternion.identity);
        PlayerPrefs.SetString("TeleportAmatista", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetFloat("TeleportAmatistaX", attackPos.position.x);
        PlayerPrefs.SetFloat("TeleportAmatistaY", attackPos.position.y);
    }
    public void SpawmTeleportTurquesa()
    {
        GameObject go = Instantiate(teleportTurquesa, attackPos.position, Quaternion.identity);
        PlayerPrefs.SetString("TeleportTurquesa", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetFloat("TeleportTurquesaX", attackPos.position.x);
        PlayerPrefs.SetFloat("TeleportTurquesaY", attackPos.position.y);
    }
    public void SpawmTeleportEsmeralda()
    {
        GameObject go = Instantiate(teleportEsmeralda, attackPos.position, Quaternion.identity);
        PlayerPrefs.SetString("TeleportEsmeralda", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetFloat("TeleportEsmeraldaX", attackPos.position.x);
        PlayerPrefs.SetFloat("TeleportEsmeraldaY", attackPos.position.y);
    }
    public void SpawmTeleportOnixBlanco()
    {
        GameObject go = Instantiate(teleportOnixBlanco, attackPos.position, Quaternion.identity);
        PlayerPrefs.SetString("TeleportOnixBlanco", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetFloat("TeleportOnisBlancoX", attackPos.position.x);
        PlayerPrefs.SetFloat("TeleportOnixBlancoY", attackPos.position.y);
    }
    public void SpawmTeleportAmbar()
    {
        GameObject go = Instantiate(teleportAmbar, attackPos.position, Quaternion.identity);
        PlayerPrefs.SetString("TeleportAmbar", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetFloat("TeleportAmbarX", attackPos.position.x);
        PlayerPrefs.SetFloat("TeleportAmbarY", attackPos.position.y);
    }
    public void SpawmTeleportJade()
    {
        GameObject go = Instantiate(teleportJade, attackPos.position, Quaternion.identity);
        PlayerPrefs.SetString("TeleportJade", SceneManager.GetActiveScene().name);
        PlayerPrefs.SetFloat("TeleportJadeX", attackPos.position.x);
        PlayerPrefs.SetFloat("TeleportJadeY", attackPos.position.y);
    }
    public void TeleportPower2(Vector3 newPos, string levelName)
    {
        if (SceneManager.GetActiveScene().name==levelName)
        {
            transform.position = newPos;
        }
        else
        {
            GameManager.instance.playerData.lastPosition = newPos;
            GameManager.instance.SaveGame(newPos, levelName);
            SceneManager.LoadScene(levelName);
        }
    }
    public void TeleportPower(Transform target)
    {
        transform.position = target.position;
        GameManager.instance.SaveGame(transform.position, nivelScene);
    }
    public void SpawmPlatformConstruction()
    {
        constructionInProgress = true;
        Instantiate(contructionPrefab, new Vector3(10000, 10000, -1), Quaternion.identity);
        Debug.Log("CreatePlatform");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (state == PlayerState.flying) //mandar codigo del ataque
        {

            anim.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
            rb2d.gravityScale = 3;
            state = PlayerState.normal;
            anim.SetBool("flying", false);
            Debug.Log("termino de volar");
        }
    }

    #region //Reseteo de la posicion de shatun a una nueva posicion
    public void RestartPos(Vector3 newPos) 
    {
        StartCoroutine(FrozenPos(newPos));
    }
    #endregion

    #region //Proceso Ejecucion de la animacion
    IEnumerator FrozenPos(Vector3 newPos)
    {
        rb2d.isKinematic = true;
        rb2d.velocity = Vector2.zero;
        state = PlayerState.frozen;
        anim.SetBool("frozen",true);
        yield return new WaitForSeconds(2.0f);
        transform.position = newPos;
        anim.SetBool("frozen", false);
        yield return new WaitForSeconds(1.0f);
        state = PlayerState.normal;
        rb2d.isKinematic = false;
    }
    #endregion

    #region //Power Up Jump Pow, golpe de arriba hacia abajo a un recipiente
    public IEnumerator SavePos()
    {
            Debug.Log("entroanimacion");
        if (currentElement != null)
        {
            rb2d.isKinematic = true;
            rb2d.velocity = Vector2.zero;
            state = PlayerState.frozen;
            if (currentElement.showAnim)
            {
                anim.SetBool("jPow", true);

            }

            yield return new WaitForSeconds(2.0f);
            if (currentElement.showAnim)
                anim.SetBool("jPow", false);
            yield return new WaitForSeconds(1.0f);
            state = PlayerState.normal;
            currentElement.GetComponent<Collider2D>().enabled = false;
            currentElement = null;
            rb2d.isKinematic = false;
        }
    }
    #endregion
}

public enum PlayerState{ //estados del jugador
    normal,
    stairs,
    flying,
    frozen
}
