using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DeathTrigger : MonoBehaviour
{
    public string levelName;
    public bool restartLevel=true;
    public Transform spawmPoint;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Player")
        {
            Debug.Log("entrartrigger");
            if (restartLevel)
            {
                SceneManager.LoadScene(levelName);
            }
            else
            {
                //collision.transform.position = GameManager.instance.playerData.lastPosition;
                
                PlayerController.instance.RestartPos(spawmPoint.position); //reseteo de la posicion de shatun al tocar el collider
            }
        }
    }

}
