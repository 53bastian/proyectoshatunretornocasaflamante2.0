using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotemsPlatform : MonoBehaviour
{
    int stones = 0;
    public Animator stonesAnim;
    public Animator platformAnim;
    public void UpdateStone()
    {
        stones++;
        if (stones == 2)
        {
            StartCoroutine(Completed());
        }
    }
    IEnumerator Completed()
    {
        stonesAnim.SetTrigger("completed");
        yield return new WaitForSeconds(1.5f);
        platformAnim.SetTrigger("completed");
    }
   
}
